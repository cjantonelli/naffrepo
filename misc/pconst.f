c		Computes potential constant for triaxial "eta" models, 
c		eta = 1
c		N.B. argument "x" is dummy.

        function pconst (x)
	implicit none
	include 'model.h.f'
	real*8 pconst, x
	integer lw,liw,ifail,key
	real*8 eta,g,al,au,epsr,epsa,alpha,beta 
	real*8 factor,out,err
	parameter (lw=2000,liw=lw/4)
	real*8 w(lw), iw(liw)
	external g
	data al /0.d0/, au /1.d0/, epsr /1.d-12/, epsa /0.d0/
	data key /2/, alpha /0.d0/, beta /0.d0/, ifail /-1/

	eta = 3. - gamma
	factor = a/dsqrt((b**2-a**2)*(c**2-a**2))
	call d01apf 
     &	  (g,al,au,alpha,beta,key,epsa,epsr,out,err,w,lw,iw,liw,ifail)
	pconst = factor*out
	
	return
	end

	real*8 function g(s)
	implicit none
	include 'model.h.f'
	real*8 s
	real*8 s2,a2,b2,c2,aa,bb

	s2 = s*s
	a2 = a*a
	b2 = b*b
	c2 = c*c
	aa = a2/(b2-a2)
	bb = a2/(c2-a2)
	g = 1.d0/dsqrt((aa + s2)*(bb + s2))
	return
	end
		

	


	

	

