c-----------------------------------------------------------------c
      subroutine rstationary(ngrid,xmax,npin)
c-----------------------------------------------------------------c
c Routine to generate a co-rotating "stationary" start space for a gamma model
c Find two coordinates on the equipotential surface in the rotating frame and
c the third coordinate so that it satisfies the Jacobi Integral.
      implicit none
      include 'model.h.f'
      include 'trio.h.f'
      integer ngrid,npts,iy,iz,isect,ifail,i,j,k,l
      integer iloop1,iloop2,ig,nmap, npin
      parameter(nmap =60)
      real*8 xmax,xga,xgb,xs,eta
      real*8 x0,y0,z0,pconst
      real*8 yoverx, zoverx,dxgrid
      real*8 xsurf(nmap,nmap),ysurf(nmap,nmap),zsurf(nmap,nmap)
     &   ,xgrid(nmap)
      real*8 errabs,errrel
      real*8 eng0, engJ,test,potential0
      real*8 xx,yy,zz,froot,pmodel
      real*8 pbhole
      real*4 x1, x2, y1, y2
      real*4 xplot(nbpt),yplot(nbpt)
      common /energy/ eng0, engJ    
      common /root/ yoverx, zoverx, isect  
      external froot,pmodel,pbhole
      integer iuser(1)
      real*8 ruser(1)
      
      eta = 3.-gamma
      dxgrid = 1.d0/dfloat(ngrid)

      xgrid(1) = dxgrid/2.d0
      do  i = 2, ngrid
         xgrid(i) = xgrid(1) + dfloat(i-1)*dxgrid
      end do
      open(32,file=modfile//'.grd',status='unknown')

      errabs = 1.d-12
      errrel = 0.d0
      do 50 iy = 1, ngrid
         do 50 iz = 1, ngrid

c		Compute, plot grid of starting values.
		yoverx = xgrid(iy)
		zoverx = xgrid(iz)
			
		do 55 isect = 1, 3
c			Compute the third coordinate
c		xga = xmax/100.d0
c		xgb = xmax*1.d0
                   xga = 0.d0
                   xgb = xmax*5.d0
		test = froot(xga,iuser,ruser)*froot(xgb,iuser,ruser)
c                
		if (test .ge. 0.d0) then
                 write(*,*) 'Decrease [Increase] xga [xgb]'
                 write(*,*) 'froot a, b', froot(xga,iuser,ruser),
     &                  froot(xgb,iuser,ruser)  
                 stop 
                end if
		ifail = -1
		call c05ayf (xga,xgb,errabs,errrel,froot,xs,iuser,ruser,
     &                  ifail)
		if (dabs(xs) .gt. 100.d0) then
			write (*,*) 'Bad root from -c05ayf-:'
			write (*,*) iy, iz, ' Xga = ', xga,'  Xsurf = ',
     &				dabs(xs)
			stop
		end if
		if (isect .eq. 1) then
			xsurf(iy,iz) = dabs(xs)
		else if (isect .eq. 2) then
			ysurf(iy,iz) = dabs(xs)
		else if (isect .eq. 3) then
			zsurf(iy,iz) = dabs(xs)
		end if

 55	continue

 50	continue

c	call pgslw (3)
	x2 = 1.15*xsurf(1,1)/sqrt(2.)
	x1 = -x2
	y1 = x1
	y2 = x2
c	call pgvsize (1.,7.5,2.5,9.)
c	call pgwindow (x1,x2,y1,y2)
c	call pgbox ('bcnt',0.,2,'bcnt',0.,2)
c        call pgenv(x1,x2,y1,y2,1,1)
	do 60 isect = 1, 3
		l = 0
		do 65 j = 1, ngrid
		do 65 k = 1, ngrid
			l = l + 1
			yoverx = xgrid(j)
			zoverx = xgrid(k)
			if (isect .eq. 1) then
				xx = xsurf(j,k)
				yy = zoverx*xx
				zz = yoverx*xx
			else if (isect .eq. 2) then
				yy = ysurf(j,k)
				xx = yoverx*yy
				zz = zoverx*yy
			else if (isect .eq. 3) then
				zz = zsurf(j,k)
				xx = zoverx*zz
				yy = yoverx*zz
			end if
			xplot(l) = (-xx + yy)/sqrt(2.)
			yplot(l) = -(xx + yy)/sqrt(6.) + sqrt(2./3.)*zz
                write(32,*) xplot(l), yplot(l)
 65		continue
c		call pgpoint (l,xplot,yplot,17)

                
60	continue
	x1 = -xsurf(1,1)/sqrt(2.)*1.05
	y1 = -xsurf(1,1)/sqrt(6.)*1.05
c	call pgtext (x1,y1,'X')
	x1 = ysurf(1,1)/sqrt(2.)*1.05
	y1 = -ysurf(1,1)/sqrt(6.)*1.05
c	call pgtext (x1,y1,'Y')
	x1 = -0.025*xsurf(1,1)
	y1 = zsurf(1,1)*sqrt(2./3.)
	y1 = 0.98*y1
c	call pgtext (x1,y1,'Z')
	x1 = ysurf(1,1)/sqrt(2.)*1.35
c	call pgslw (2)
c        call pgadvance

        close(32)
c npin should normally be zero; this is set to npin in case you do
c both tube and box orbits
        npts=npin
	do 500 isect = 1, 3
	do 500 iloop1 = 1, ngrid
	do 500 iloop2 = 1, ngrid

		yoverx = xgrid(iloop1)
		zoverx = xgrid(iloop2)
		if (isect .eq. 1) then
			x0 = xsurf(iloop1,iloop2)
			y0 = zoverx*x0
			z0 = yoverx*x0
		else if (isect .eq. 2) then
			y0 = ysurf(iloop1,iloop2)
			x0 = yoverx*y0
			z0 = zoverx*y0
		else if (isect .eq. 3) then
			z0 = zsurf(iloop1,iloop2)
			x0 = zoverx*z0
			y0 = yoverx*z0
		end if
	potential0 = pmodel(x0,y0,z0)+pbhole(x0,y0,z0)+const
     &         -0.5d0*omega**2*(x0**2+y0**2)
		test = dabs(engJ - potential0)
		if (test .gt. 1.d-8) then
                   write (*,*) isect,iloop1, iloop2, test,
     &					' Not on equipotential'
                else
                   npts = npts+1
                   xx0(1,npts) = x0
                   xx0(2,npts) = y0
                   xx0(3,npts) = z0
                   xx0(4,npts) = 0.d0
                   xx0(5,npts) = 0.d0
                   xx0(6,npts) = 0.d0
                   write(12,121) npts,isect,iloop1,iloop2,x0,y0,z0
                end if
                npin= npts
 500            continue
 121            format(4(1x,i2),3(2x,f8.4))
       return
       end
c------------------------------------------------------------
      real*8 function froot (x,iuser,ruser)
c--------------------------------------------------------
      implicit none
      include 'model.h.f'
      include 'trio.h.f'
      real*8 x,eng0,yoverx, zoverx,xx,yy,zz,pmodel
      real*8 pbhole,engJ 
      integer isect
      common /energy/ eng0 , engJ
      common /root/ yoverx, zoverx, isect
      external pmodel,pbhole
      integer iuser(*)
      real*8 ruser(*)

      if (isect .eq. 1) then
		xx = dabs(x)
		yy = xx*zoverx
		zz = xx*yoverx
	else if (isect .eq. 2) then
		yy = dabs(x)
		xx = yy*yoverx
		zz = yy*zoverx
	else if (isect .eq. 3) then
                zz = dabs(x)
		xx = zz*zoverx
		yy = zz*yoverx
	end if
	froot = engJ - (pmodel(xx,yy,zz)+pbhole(xx,yy,zz)+const)
     &         +0.5d0*omega**2*(xx**2+yy**2)

	return
	end
     
