c--------------------------------------------------------------------c
      subroutine mass
c--------------------------------------------------------------------c
c      Monica Valluri, 
c      University of Chicago
c      Sept 2003, Converted to Subroutine and integrated into main program

c     This routine generates model "Mass distribution" (on a polar grid)
c     for Schwarzschild's orbit superposition routine.
c
c--------------------------------------------------------------------c
      include 'schwarz.h.f'
      integer i,j,ifail
      real*8 cerr(nrad,nthgd)
      real*8 c1
      real*8 mtot1
      integer ndim, Minpts, maxpts,lenwrk
      parameter(ndim = 2, maxpts = 50000*ndim, LENWRK =(NDIM+2)*
     & (1+MAXPTS/(2**NDIM+2*ndim*ndim+2*ndim+1)))
      real*8 AA(ndim),BB(NDIM),eps,functn,wrkstr(lenwrk)
     & ,final,acc
      external functn,D01FCF

c Open file to write output data to:
      open(9,file='../datafiles/mass.dat',status='unknown')
      write(9,*)'MASS DISTRIBUTION: for run  ', outfile 
      write(9,*)'M/L =  ',  mbyl
      write(9,*)'inclination ', incl
      write(9,*) 'dmin, rout, dout', dmin, rout, dout
      write(9,*) 'grid size: nrad,  nthgd',  nrad, nthgd

c Radial grid set up by main (define.f)
c----------------------------- CELL MASSES ------------------------------c
c 
      open(81,file='../datafiles/massgrid.dat',status='unknown')
      do i = 1,nrad
         write(81,*) i, radi(i)
      end do
      do j = 0,nthgd
         write(81,*) j, thetaj(j)
      end do
      c1 = 4.d0*pi
      close(81)
c
      mtot1= 0.d0
      do i = 1,nrad
            AA(1) = radi(i-1)
            BB(1) = radi(i)
         do j = 1,nthgd
            AA(2) = thetaj(j-1) 
            BB(2)= thetaj(j) 
            ifail = -1
            minpts = 0
            eps = 1.d-13
            call D01FCF(ndim,AA,BB,MINPTS,MAXPTS,FUNCTN,EPS,
     &           ACC,LENWRK,WRKSTR,FINAL,IFAIL)
            if(ifail.ne.0) write(*,*) 'ifail', ifail
            masmod(i,j) = sngl(c1*final)
            vol(i,j) = sngl(c1*(bb(1)**3-aa(1)**3)*
     &                (dcos(aa(2))-dcos(bb(2))))
            cerr(i,j) = acc
c            write(*,*) i,j, masmod(i,j), acc
            mtot1 = mtot1+masmod(i,j)
         end do
      end do
      write(*,*)'Total Mass of galaxy in rout',mtot1*Mtilde, 'Msun'
      write(9,*)'Total Mass of galaxy in rout',mtot1*Mtilde, 'Msun'
c
c---------------------Write output data to file -------------------c           
c Write out cell masses:
      write(9,*) 'CELL MASSES'
      write(9,*) nrad*nthgd
      do i = 1,nrad
         do j = 1,nthgd
            write(9,*) i,j, masmod(i,j), vol(i,j), masmod(i,j)/vol(i,j)
         end do
      end do
      write(9,*) '        '

      close(9)
c------------------------------------------------------------------c
      return
      end

c------------------------------------------------------------------c
      real*8 function functn(NDIM,Z)
      implicit none
      integer NDIM
      real*8 z(2), R, zz,sinth, rho, mge_dens
      external rho, mge_dens
      
      sinth = dsin(z(2))
      R = z(1)*sinth
      zz = dsqrt(z(1)**2-r**2)
c      write(*,*) 'rho, mge_dens', rho(r,zz), mge_dens(r,zz)
c      functn = rho(r,zz)*sinth*z(1)**2
      functn = mge_dens(R,zz)*sinth*z(1)**2

      return
      end
