c---------------------------------------------------------------------------c
c Find period of long axis on triaxial gamma potential
	subroutine period(xmaxi)
c---------------------------------------------------------------------------c
	include 'model.h.f'
	real*8 eta,emaxi,timei,al,au,epsr,epsa
	integer lw,liw,nlimit
	real*8 pbhole
	real*8 funct,res,abser,xmaxi,pmodel
	parameter(lw=2000, liw=lw/4)
	real*8 w(lw)
	integer iw(liw), ifail
	common /turnpt/ emaxi,timei  
	external pmodel,d01ajf, funct,pbhole

	emaxi=pmodel(xmaxi,0.d0,0.d0)+pbhole(xmaxi,0.d0,0.d0)+const
	al = 0.d0
	au = xmaxi 
	epsr = 1.d-7
	epsa = 0.d0
	nlimit = -1
	ifail = -1
	call d01ajf(funct,al,au,epsa,epsr,res,abser,w,lw,iw,liw,ifail)
	if (ifail .ne. 0) write (*,*) 'Failure to compute time'
	timei=2.d0*dsqrt(2.d0)*res
	
	return
	end

c----------------------------------------------------------------------------c
	function funct(x)
	include 'model.h.f'
	real*8 funct,x,emaxi,timei
	real*8 pmodel,v,pbhole
	common /turnpt/ emaxi,timei
	external pmodel, pbhole

	v=pmodel(x,0.d0,0.d0)+pbhole(x,0.d0,0.d0)+const
	
	funct=1.d0/(dsqrt(emaxi-v))
	return
	end
	
c-------------- -------------------------------------------------------------c

