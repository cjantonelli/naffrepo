c  Computes potential for triaxial "gamma" models.
*======================================================================
*       pgamma.f
*======================================================================

	REAL*8 FUNCTION pmodel (x,y,z)
	implicit none
	include 'model.h.f'
	integer ifail, np, nlimit
	real*8 x, y, z, x2, y2, z2, g1, g2
	real*8 al, au, epsr, err, d01ahf      
	common /fpot/ x2, y2, z2
	external g1, g2, d01ahf
      
	x2 = x*x
	y2 = y*y
	z2 = z*z
c	write(*,*) 'x,y,z,a,b,c,gamma',x,y,z,a,b,c,gamma

	al = 0.d0
	au = 1.d0
	epsr = 1.d-10
	nlimit = -1
	ifail = -1
	if (gamma .eq. 2.d0) then
		pmodel = -a*d01ahf (al,au,epsr,np,err,g1,nlimit,ifail)
	else
		pmodel = -a*d01ahf (al,au,epsr,np,err,g2,nlimit,ifail)
		pmodel = pmodel/(2.d0-gamma)
	end if
	return
	end
c
c
	REAL*8 FUNCTION g1(s)
	include 'model.h.f'
	real*8 x2, y2, z2, s2, a2, b2, c2
	real*8 term, s, em, tmp
	common /fpot/ x2, y2, z2

	s2 = s*s
	a2 = a*a
	b2 = b*b
	c2 = c*c
	term = dsqrt(x2/a2 + y2/(a2 + (b2-a2)*s2) + z2/(a2 + (c2-a2)*s2))
	em = s*term
	g1 = (dlog((1.d0+em)/term) - 1.d0/(1.d0+em))/
     &		dsqrt((a2 + (b2-a2)*s2)*(a2 + (c2-a2)*s2))
	return
	end

	real*8 function g2(s)
	implicit none
	include 'model.h.f'
	real*8 s, x2, y2, z2,s2,a2,b2,c2,em2,em,fact
	common /fpot/ x2, y2, z2
	s2 = s*s
	a2 = a*a
	b2 = b*b
	c2 = c*c
	em2 = s2*(x2/a2 + y2/(a2 + (b2-a2)*s2) + z2/(a2 + (c2-a2)*s2))
	em = dsqrt(em2)
	fact = em/(1.d0 + em)
	g2 = (1.d0 - (3.d0-gamma)*fact**(2.d0-gamma) + 
     &		(2.d0-gamma)*fact**(3.d0-gamma))/
     &		dsqrt((a2 + (b2-a2)*s2)*(a2 + (c2-a2)*s2))
	return
	end

