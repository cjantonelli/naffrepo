*======================================================================
*       fgamma2.f
*======================================================================
       
c       Computes components of force for triaxial "gamma" models 
c	Uses non-adaptive, Gaussian rule [D01BAF].
c	N.B. D01BAZ must be declared "external".
      
      SUBROUTINE fmodel(x,y,z,fx,fy,fz)
      implicit none
      include 'model.h.f'
      integer ifail,nlimit
      real*8 x,y,z,fx,fy,fz
      real*8 eta, aa1, aa2, cc1, cc2, cc3, ai2 
      real*8 x2, y2, z2, func1
      real*8 al, au, epsr, termx, termy, termz, relerr
      real*8 a2, b2, c2, d01baz, d01baf       

      common /factors/ aa1, aa2, cc1, cc2, cc3, ai2, x2, y2, z2
      external func1, d01baz, d01baf
      
c		At origin?

	IF (x*x+y*y+z*z .lt. 1.d-10) THEN

        fx = 0.d0
        fy = 0.d0
        fz = 0.d0

	
	return
	ENDIF
	a2 = a*a
	b2 = b*b
	c2 = c*c      
	x2 = x*x
	y2 = y*y
	z2 = z*z
      
	al = 0.d0
	au = 1.d0
	epsr = 1.d-10
	nlimit = -1

c		Fx

	aa1 = b2 - a2
	aa2 = c2 - a2
	cc1 = 0.d0
	cc2 = b2 - a2
	cc3 = c2 - a2
	ai2 = a2

	ifail = -1
	termx = d01baf (d01baz,al,au,32,func1,ifail)
	eta = 3.d0 - gamma
	fx = -eta*(x/a)*termx
	if (ifail .ne. 0) write (*,*) 'Failure to compute Fx'

c		Fy

	aa1 = c2 - b2
	aa2 = a2 - b2
	cc1 = a2 - b2
	cc2 = 0.d0
	cc3 = c2 - b2
	ai2 = b2
	
	ifail = -1
	termy = d01baf (d01baz,al,au,32,func1,ifail)
	fy = -eta*(y/b)*termy
	if (ifail .ne. 0) write (*,*) 'Failure to compute Fy'

c		Fz

	aa1 = a2 - c2
	aa2 = b2 - c2
	cc1 = a2 - c2
	cc2 = b2 - c2
	cc3 = 0.d0
	ai2 = c2

	ifail = -1
	termz = d01baf (d01baz,al,au,32,func1,ifail)
	fz = -eta*(z/c)*termz
	if (ifail .ne. 0) write (*,*) 'Failure to compute Fz'

	return
	end

	real*8 function func1(s)
        implicit none
        include 'model.h.f'
	real*8 s,aa1, aa2, cc1, cc2
	real*8 cc3, ai2, x2, y2, z2,s2,em2,em
	common /factors/ aa1, aa2, cc1, cc2, cc3, ai2, x2, y2, z2

	s2 = s*s
	em2 = s2*(x2/(ai2+cc1*s2) + y2/(ai2+cc2*s2) + z2/(ai2+cc3*s2))
	em = dsqrt(em2)
	func1 = s2/em**gamma/(1.d0+em)**(4.d0 - gamma)/
     &		dsqrt((ai2 + aa1*s2)*(ai2 + aa2*s2))
	return
	end


