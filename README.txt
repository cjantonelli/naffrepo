This repository contains source code, documentation and examples for orbital frequency analysis and orbit classification.

The directory Docs contains Buildnotes.txt with instructions on how to install and build the codes. The directory also contains NAFF_Docmentation.pdf with notes on how to use the code.
