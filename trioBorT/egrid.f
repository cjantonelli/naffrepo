c subroutine to generate the grid of ellipsoidal shell with
c equal masses for the ellipsoidal gamma model
c program computes, radial grid, energy, jacobi energy, 
c period of long axis orbit
c in rotating frame

      subroutine egrid
      implicit none
      include 'trio.h.f'
      include 'model.h.f'
      include 'fmarray.f'
      integer ishell
      real*8 eta,al,au,fa,fb,fgrid,res,xmaxi,ieta
      real*8 x,x1, pconst, emaxi, timei
      common /turnpt/ emaxi,timei
      common /index/ ishell
      external  period, pmodel,pconst

      eta = 3.d0-gamma
      ieta = 1.d0/eta
c
      if(eta.eq.1.d0) then
         const = pconst(1.d0)
      else
         const = 0.d0
      end if
c note replaced ellipsoidal coordinate mi notation with radi to avoid
c confusion with m=mass.

      x1 = dfloat(nrad+1)**ieta
      do ishell=1,nrad
	   x = dfloat(ishell)**ieta
           radi(ishell) = x*a/(x1-x)
c           write(*,*) 'i, ellipsoidal radius ', ishell, radi(ishell)
      end do
	
c  calculate energy and period of the radial orbit at each value of radi
      open(21,file=trim(freqfile)//'_egrd.dat', status='unknown')
      write(21,*) 'i   r(i), period(i), E(i), EJ(i)'
      do ishell=1,nrad
	  xmaxi = radi(ishell)
	  call period(xmaxi)
	  energy(ishell) = emaxi
	  time(ishell) = timei
c jacobi Integral:
c select the value of the Jacobi Integral to be the value for a stationary 
c point at the point of intersection of the x-axis with the equipotential
c surface
          jacobi(ishell) = energy(ishell)-0.5*(omega*xmaxi)**2
          write(21,*) ishell,radi(ishell),time(ishell),energy(ishell),
     &          jacobi(ishell)
      end do
      close(21)
c
      return
      end
