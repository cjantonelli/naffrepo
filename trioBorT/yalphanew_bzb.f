
c-------------------------------------------------------------------c
      subroutine yalpha(ngrid,xmax,npts)
c-------------------------------------------------------------------c
c Nov 16, 2017: This version replaces NAG routine C05ADF with
c    BrentRoots from Borland Mathematical Library at:
c http://jean-pierre.moreau.pagesperso-orange.fr/Fortran/zbrent_f90.txt
c MV changed this to be acceptable to the gfortran Fortran77 compiler
c Routine to generate rotational start space for a gamma model
      implicit none
      include 'model.h.f'
      include 'trio.h.f'
      include 'fmarray.f'
      integer ngrid,npts,i,j,k,l,ierr,maxiter,kiter
      real*8 xmax,ymax,dygrid
      real*8 errabs,errrel,xa,xb,xs
      real*8 eng0,engJ,test, yr
      real*8 y,froot1,pmodel, BrentRoots
      real*8 yi,vel,alphai,alpha(npartmax),vxi,vzi,dalpha
      real*4 x1,x2,y1,y2
      real*4 xplot,yplot
      common /energy/ eng0, engJ    
      external froot1,pmodel,BrentRoots
c      write(*,*) 'inside xzrst ngrid, xmax', ngrid, xmax

c Find ymax:      
      x1 = 0.
      x2 = sngl(xmax*1.2)
c      xa = xmax/1.d+4
      xa = 0.d0
      xb = 1.d0*xmax
c      errabs = 1.d-12
      errrel = 1.d-10
      maxiter = 50
c      write(*,*) 'froota, frootb', froot1(xa), froot1(xb)
	test = froot1(xa)*froot1(xb)
	if (test .ge. 0.d0) stop 'Decrease [Increase] xa [xb]'

c	call c05adf (xa,xb,errabs,errrel,froot1,xs,ifail)
        xs = BrentRoots(xa, xb, froot1,errrel,maxiter,
     &       yr, kiter, ierr)
         if(ierr.eq.1) stop 'no root found in interval'
         if(ierr.eq.2) stop 'increase maxiter in rstationary'
        ymax = xs
c        write(*,*) 'ymax', ymax
        dygrid = ymax/dfloat(ngrid)
c----------------------------------------------------------------c
c  Start at center and go outwards:
        npts = 0
        yi = dygrid*0.5d0
        dalpha = 0.5*pi/dfloat(ngrid-1)
c First loop over y values
        do i = 1,ngrid
           vel = EngJ - (pmodel(0.d0,yi,0.d0) + const)
     &                + 0.5d0*(omega*yi)**2
           vel = dsqrt(2.d0*vel)
c loop over alpha values:
           alphai = 0.d0
           do j = 1,ngrid
              vxi = vel*dcos(alphai)
              vzi = vel*dsin(alphai)
              alphai = alphai+dalpha
              npts = npts +1
              xx0(1,npts) = 0.d0
              xx0(2,npts) = yi
              xx0(3,npts) = 0.d0
  	      xx0(4,npts) = vxi
              xx0(5,npts) = 0.d0
              xx0(6,npts) = vzi
              alpha(npts) = alphai
              write(13,*) npts, sngl(yi), sngl(alphai), sngl(vel)
c           write(*,*) npts, yi, vxi, vzi
           end do
           yi = yi + dygrid
        end do

c plot this initial start space:
        
c        call pgbegin(0, 'yalpha_start.ps/vps',1,1)
c	call pgslw (3)
        x1 = 0.
        x2 = sngl(xmax)
        y1 = 0.
c        y2 = (0.5*pi)*(180/pi)
        y2 = 90.
c        call pgenv(x1,x2,y1,y2,0,1)
c        call pglabel('y', '\ga\d0\u', 'Y-\ga start space')
        open(3,file='yalpha_start.dat')
        write(3,*) 'Y0   ',      ' alpha (degrees)'
        do i = 1,npts
           xplot = sngl(xx0(2,i))
           yplot = sngl(alpha(i)*180./pi)
           write(3,*) xplot, yplot
c           call pgpt(1,xplot,yplot,17)
        end do

        close(3)
        return
        end

c----------------------------------------------------------------c
c
      function froot1(x)
      implicit none
      include 'model.h.f'
      include 'trio.h.f'
      real*8 froot1, x, zero,engJ,eng0,pmodel
      real*4 xp(1), yp(1)
      common /energy/ eng0,engJ    
      external pmodel

      zero = 0.d0
        froot1 = (pmodel(zero,x,zero) + const)-
     &          0.5d0*(omega*x)**2-EngJ
        xp(1) = sngl(x)
        yp(1) = sngl(froot1)
c        call pgpt(1,xp, yp, 20)
      return
      end
c
c----------------------------------------------------------------c

