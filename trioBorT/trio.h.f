c------------------------ trio.h.f ------------------------------------
c     former version had overlap with FMarray.f (common arrays for naff.f)
c     this version has commons only used in TrioBorT
c----------------------------------------------------------------------
c Parameters to be set at input:

c     nbpt: maximum number of orbits that can be integrated (must=npartmax in
c           fmarray.f
c     norb: number of orbital periods for which orbit is integrated

      integer ndat, nbpt, norb, npbox, nptube
      integer nrad, nthgd, nphi, nvar, ngrbx, ngrxz
      parameter(nrad = 40, nvar = 6, nthgd=8, nphi=8, nbpt=10000)
      parameter(norb=50)  
   
      real*8 xx0(6, nbpt), inttime(nbpt)
      common /initial/ xx0, inttime
      common /size/ ndat, npbox, nptube, ngrbx, ngrxz

C Radial, mass, energy, period grids:
      real*8 energy(nrad), jacobi(nrad), time(nrad)
      real*8 radi(nrad), thetaj(0:nthgd), phiaz(0:nphi)
      common /rad/ radi, energy, jacobi, time, thetaj
      

	  
	  
	  
