c THIS FIlE CONTAINS ROUTINES TO COMPUTE THE FORCE Components 
C AND POTENTIAL DUE TO A BLACK HOLE OF MASS bhm

c		Subroutine -fbhole.f-
c
c		Computes components of force due to a Black Hole of mass bhm   
c		If desired, also computes derivatives of forces.

	subroutine fbhole(x,y,z,fx,fy,fz)
	implicit none
	include 'model.h.f'
	real*8 x,y,z,fx,fy,fz,bhmm,eps22,x2,y2,z2,r
	common /bhmass/ bhmm
	common /softn/ eps22

	bhmm = bhm
	eps22 = eps2
	if(bhm.eq.0.d0) then
	   fx = 0.d0
	   fy = 0.d0
	   fz = 0.d0
	   return
	end if

	x2 = x*x
	y2 = y*y
	z2 = z*z
	r=dsqrt(x2+y2+z2+eps2)
	
c		Fx
	fx= -bhm*x/(r**3)
c               Fy
	fy= -bhm*y/(r**3)
c               Fz
	fz= -bhm*z/(r**3)

	return
	end

c ----------------------- potential due to black hole -----------------

	function pbhole(x,y,z)
	implicit none
	real*8 pbhole,x,y,z,bhm,eps2,r

	common /bhmass/ bhm
	common /softn/ eps2

	if(bhm.eq.0.d0) then
	   pbhole = 0.d0
	   return
	end if
	r = dsqrt( x**2 + y**2 + z**2 + eps2)
	pbhole = - bhm/r
	
	return
	end	
c---------------------------------------------------------------------
