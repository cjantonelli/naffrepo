c		Computes potential constant for triaxial "eta" models, 
c		eta = 1
c		N.B. argument "x" is dummy.

        function pconst (x)
	implicit none
	include 'model.h.f'
	real*8 pconst, x
	integer lw,last,neval,key,ier
	real*8 eta,g,al,au,epsr,epsa,alpha,beta 
	real*8 factor,out,abserr
	parameter (lw=2000)
	real*8 alist(lw), blist(lw), rlist(lw), elist(lw) 
        integer iord(lw)
	external g
	data al /0.d0/, au /1.d0/, epsr /1.d-12/, epsa /0.d0/
	data key /2/, alpha /0.d0/, beta /0.d0/

	eta = 3. - gamma
	factor = a/dsqrt((b**2-a**2)*(c**2-a**2))

c	call d01apf 
c     &	  (g,al,au,alpha,beta,key,epsa,epsr,out,err,w,lw,iw,liw,ifail)
	call dQAWSE
     &	  (g,al,au,alpha,beta,key,epsa,epsr,lw,out,abserr,neval,ier,
     &    alist,blist,rlist,elist,iord,last)

	pconst = factor*out
	
	return
	end

	real*8 function g(s)
	implicit none
	include 'model.h.f'
	real*8 s
	real*8 s2,a2,b2,c2,aa,bb

	s2 = s*s
	a2 = a*a
	b2 = b*b
	c2 = c*c
	aa = a2/(b2-a2)
	bb = a2/(c2-a2)
	g = 1.d0/dsqrt((aa + s2)*(bb + s2))
	return
	end
		

	


	

	

