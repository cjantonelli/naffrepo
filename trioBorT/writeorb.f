      subroutine writeorb(np)
      implicit none
      include 'trio.h.f'
      include 'fmarray.f'
      integer np, i,j,npl
      real*4 xx(nbmax), yy(nbmax), zz(nbmax)
      real*4 vxx(nbmax), vyy(nbmax), vzz(nbmax)
      real*4 x1,x2,y1,y2,z1,z2
      character*4 ono

      i=0
      do j = 1,nbmax
         i=i+1
         xx(i) = sngl(dreal(fkx(j)))
         yy(i) = sngl(dreal(fky(j)))
         zz(i) = sngl(dreal(fkz(j)))
         vxx(i) = sngl(dimag(fkx(j)))
         vyy(i) = sngl(dimag(fky(j)))
         vzz(i) = sngl(dimag(fkz(j)))
      end do
      npl = i
c FInd maximum and  minimum of x,y,z
      x1 = 1.e+10
      x2 = -1.e+10
      y1 = 1.e+10
      y2 = -1.e+10
c
      do i = 1,npl
         x1 = min(x1, xx(i))
         x2 = max(x2, xx(i))
         y1 = min(y1, yy(i))
         y2 = max(y2, yy(i))
      end do

      x2 = max(abs(x1),abs(x2))
      y2 = max(abs(y1),abs(y2))
      x2 = max(x2, y2)

      y2 = x2
      x1 = -x2
      y1 = -y2
        
      x1 = x1*1.1
      x2 = x2*1.1
      y1 = y1*1.1
      y2 = y2*1.1
c        write(*,*) 'x1, x2, y1, y2 ', x1, x2, y1, y2

c write the solution:
      write(19,*) npl
      write(19,*) x1,x2,y1,y2
      do i = 1,npl
         write(19,123) xx(i),yy(i), zz(i),vxx(i),vyy(i), vzz(i)
      end do
 123  format(6(e14.6,2x))
      return
      end
