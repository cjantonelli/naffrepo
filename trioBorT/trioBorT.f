cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c TrioBorT.f : Trio version that will do either box (stationary start space)
c or tube (yalpha start space)
c
      Program TrioBorT
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c This is version 3.0: version is for Box or Tube orbits
c
c Version 2.0: Figure rotation of triaxial ellipsoid with central SMBH
c               coriolisis forces and centrifugal forces
c               Correspondingly includes rotational start space
c               (Schwarzschild 1982)
c Uses modified force routines which  are faster but less accurate
c for the  non integral values of gamma
c FORCE ROUTINE FGAMA2.f for cuspy gamma (Dehnen) model. 
c Stationary start space initial conditions for box orbits, 
c and yalpha start space for tubes+boxes
cc old- x-z start space for tube (and some box) orbits
c Main program for generating orbit libraries for Triaxial, Rotating, 
c cuspy models with central SMBH.
c
c
c OUTLINE:
c ----------
C 1)  Generate a grid of shells of equal mass and for each shell:
c      - compute xaxis intersection (radi(nrad))
c      - compute long-axis period of time(nrad)
c      - compute equipotential energy energy(nrad)
c      - compute Jacobi integral
c 2)  Generate initial conditions for orbits
c      - Box-lilke orbits with zero-velocity on Jacobi-I surface. 
c      - x-z start space: with energy of Jacobi integral
c        as in Schwarzscild 1979.
c 3)  Integrate each orbit from the grid of initial conditions for  
C     norb~200 orbital  periods. (ORBINT)
c 4)  store orbital masses on mass storage grid
c 5)  store orbital kinematics on kinematic grid for 4 different projections
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c Variable declaration:
c
      implicit none
      include 'trio.h.f'
      include 'model.h.f'
      include 'fmarray.f'
      integer nx,ny,i,j,np,istat,m,tsl,irs,nflg
      integer splot,npl, ishell, ngrid, npts
      real*8 x0(nvar),Vmax,Vx,Vy,delv,phi0,ti1,ti2
      real*8 per,getcpu,time0,timen     
      real*8 eta,pmodel,eng0,engJ
      real*8 pbhole,corotr
      character*1 yinit, ans, ansstart
      real*8 xmax,ca1,triax1,gamma1, bhm1
      real*8 errabs,errrel
c freq mapping variables:
      real*8 nui(3), nu1(3,npartmax), nu2(3,npartmax)
      real*8 df1(npartmax), df2(npartmax), df3(npartmax), diff(npartmax)
      complex*16 aai(3)
      complex* 16 aa1(3,npartmax), aa2(3,npartmax)
      character*10 fileref
      common /energy/ eng0,engJ      
      common /state/ istat
      common /timslice/ tsl
      external per, pmodel, getcpu
      external orbint, plotorb
      external fundafreq, energy_cons
      external writeorb
c----------------------------------------------------------------------c
c  READ INITIAL CONDITIONS:
c      write (*,*) 'c/a, T, gamma:'
c     read (*,*) c, Triax, gamma
c     check that nbpt is ge. npartmax:
      if(nbpt.lt.npartmax) stop 'set nbpt=npartmax in trio.h.f'
      c= 0.5d0
      Triax=0.5d0
      gamma=1.d0
      eta = 3.d0-gamma
      a = 1.d0
      b = dsqrt(1.d0-Triax*(1.d0-c**2))
c Note       triax = (1.d0-b**2)/(1.d0-c**2)
      bhm=0.d0
c Uncomment next two lines to add BH
c      write(*,*) 'Blackhole mass?'
c      read(*,*) bhm

c      write(*,*) 'blackhole softening parameter?'
c      read(*,*) eps2
      eps2 = 1.d-4
      eps2 = eps2**2
c Initialize rotation:
      omega=0.d0
c Default is no rotation:
      ans='n'
c Uncomment next two lines to add  rotation
c      write(*,*) 'Add Rotation ( yes/  no)?'
c      read(*,'(a1)')  ans
      if(ans.eq.'y') then
         yrot=.true.
      else
         yrot=.false.
      end if

      if(yrot) then
         write(*,*) 'Co-rotation radius in units of a (e.g. 25, 50)?'
         read(*,*) corotr
         omega = (1.d0/corotr)**gamma*(1.d0+corotr)**(gamma-3.d0)
         omega = dsqrt(omega)
c	tmax = (2.d0*pi/omega)*6.35d+5
      end if
c Define filename for output files:
      write(*,*) 'Give filename for output files (a1..7)'
      read(*,'(a7)') freqfile

c ------------------- define grids  -----------------------------------c
c of equal mass shells
c compute: x-axis intercepts (radii) 
c energy grid
c jacobi integral for each surface
c period of long-axis orbit
      call egrid
      write(*,*) 'done with egrid'
C---------- GENERATE GRID OF INITIAL CONDITIONS: -----------------------c

c initialize initial conditions for all orbits:
      do i = 1,npartmax
         do j = 1,nvar
            xx0(j, i) = 0.d0
         end do
      end do
      open(19, file = trim(freqfile)//'.orb', status='unknown')
      open(9, file = trim(freqfile)//'.out', status='unknown')

      write(9,*) 'frequency mapping for '
      write(9,*) 'a, b, c, T ', a, b, c, triax
      write(9,*) 'M_bh, eps ', bhm, dsqrt(eps2)
      write(9,*) 'Rotation ', yrot, omega

      open(10, file= trim(freqfile)//'_naff.dat', status='unknown')
      open(55, file= trim(freqfile)//'.frq',status='unknown')

c uncomment next two lines to change shell #
c      write(*,*) 'give shell number for orbits (less than', nrad, ' )'
c      read(*,*) ishell
      ishell = 8
      eng0 = energy(ishell)
      engJ = jacobi(ishell)
      xmax = radi(ishell)
      write(*,*) 'eng0, engJ, xmax', eng0, engJ, xmax

      write(*,*) 'Do you want Box start or Tube start? (B/T)'
      read(*,'(a)') ansstart
      npts = 0
      if(ansstart.eq.'T'.or.ansstart.eq.'t') then 
c     Y-Alpha Tube orbit start space:
c Uncomment next two to change orbit number
c       write (*,*) 'Grid size for y-alpha start space [less than 101]:'
c      read(*,*) ngrxz
         open(13,file=trim(freqfile)//'_yalph.dat',status='unknown')
         ngrxz=8
         ngrid = ngrxz
         if (ngrid .gt. 100) stop 'Grid Too large'
         Write(*,*) 'GENERATING Tube (Y-alpha) START SPACE'
         nptube = ngrid**2
         write(*,*) nptube,' initial conditions will be generated '
c         call xzrstart(ngrid,xmax,npts)
         call yalpha(ngrid,xmax,npts)
         write(*,*) npts,' tubes+boxes on shell ',ishell
         close (13)
      else
c     stationary start space :    
c      uncomment next two lines to change library size
c      write (*,*) 'Grid size for stationary start space [< 57]:'
c      read (*,*) ngrbx
         ngrbx=6
c First box start space:
         ngrid = ngrbx
         if (ngrid .gt. 57) stop 'Grid too large'
         npbox = ngrid**2*3
         write(*,*) npbox, 'Box orbit ICs generated on each shell'
         open(12, file= trim(freqfile)//'_bxst.dat', status='unknown')
         call rstationary(ngrid,xmax,npts)     
         write(*,*) npts, ' boxes on shell ',ishell 
         close(12)
      end if

      do j= 1,npts
           inttime(j)= time(ishell)*dfloat(norb)
      end do

      write(55,*) time(ishell)


c Number of initial values generated:
      open(11,file =trim(freqfile)//'.init',status='unknown')
      write(11,*) npts
      do np = 1,npts
         write(11,*)sngl(inttime(np)),(sngl(xx0(j, np)), j = 1,nvar)
      end do
      close(11)

      write(*,*) 'Number of initial conditions generated ', npts


cc---------------------------------------------------------------c
c line for opening plot file:
c uncomment next two lines to allow for writing orbits to be plotted
c      write(*,*) ' write filename.orb to Plot orbits? (y/n)'
c      read(*,'(a)') ans
      ans='n'
      if(ans.eq.'y') then
         splot= 1
c         call pgbegin(0,trim(freqfile)//'_orb.ps/vps',3,4)
      end if

c now start generating orbits:
c      write(*,*) 'Orbit to start at (1 for new run)'
c      read(*,*) irs
      irs=1
      istat = 0
      time0 = getcpu()
      write(19,*) npts, ndat
      do np = irs,npts
c Graceful exit:
c         if(jmod(np,20).eq.0) then
c            open(12,file = trim(freqfile)//'.flg',status='old')
c            read(12,*)  nflg
c            close(12)
c            if(np.gt.nflg) then
c               write(*,*) '# of pts > value in flag file',nflg
c               stop
c            end if
c         end if
         do j = 1,6
            x0(j) = xx0(j,np)
         end do
         ti1 = getcpu()
c----------------- Main integrator routine: -------------------------------c
         tmax = inttime(np)
         call orbint(x0)
         ti2 = getcpu()
         if(np.eq.1) write(*,*)'ORBIT: ', np,' time',sngl(ti2-ti1)
         if(mod(np,20).eq.0) write(*,*)'ORBIT: ', np,' time',
     &        sngl(ti2-ti1)
         tsl = 0
         ti1 = getcpu()
c-------------------------------------------------------------------c
c plot every 10th orbit:
c         if(np.eq.1.or.mod(np, 10).eq.0) then
            if(splot.eq.1) call writeorb(np)
c         end if
c--------Check energy conservation for orbit:-----------------------c
c        e0 = energy_cons(1)
c        ef = energy_cons(ndat)
c        write(60,*) np, dabs((ef-e0)/e0)
c-------- Main frequency analysis routine: --------------------------------c
c
         call fundafreq(1,ndat/2,nui,aai)
         do j=1,3
            nu1(j,np) = nui(j)
            aa1(j,np) = aai(j)
         end do

         tsl = 1
         call fundafreq(ndat/2+1,ndat,nui,aai)

         do j=1,3
          nu2(j,np) = nui(j)
          aa2(j,np) = aai(j)
         end do 
c         write(10,*) np, (nu2(j,np),j=1,3), 
c     &                (cdabs(aa2(j,np)),j=1,3)

c diffusion rates for this orbit:
         df1(np) = dabs(nu1(1,np)-nu2(1,np))
         df2(np) = dabs(nu1(2,np)-nu2(2,np))
         df3(np) = dabs(nu1(3,np)-nu2(3,np))
         diff(np) = max(df1(np), df2(np))
         diff(np) = max(diff(np), df3(np))

        
         write(10,*)(nu1(j,np),j=1,3)
         write(55,261) np, (nu1(j,np),j=1,3),
     &                 (cdabs(aa1(j,np)),j=1,3)
         write(55,261) np, (nu2(j,np),j=1,3),
     &                 (cdabs(aa2(j,np)),j=1,3)
         
        
         ti2 = getcpu()
c         if(mod(np,20).eq.0) write(*,*)'Time for freq analysis', ti2-ti1
         ti1 = getcpu() 
         istat = 1
      end do
 261  Format(i4,2x,3(f12.8,2x),3(f12.8,2x))

c----End of loop over initial values -------------------------------------c
      timen = getcpu()-time0
      write(*,*) 'Total CPU time', timen
      close(9)
      close(10)
      close(55)
      close(56)

c------------------------------END OF MAIN -------------------------------c

      stop
      end
