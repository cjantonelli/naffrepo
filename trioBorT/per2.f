c---------------------------------------------------------------------------c
c Find period of long axis on triaxial gamma potential
	subroutine period(xmaxi)
c---------------------------------------------------------------------------c
	implicit none
	include 'model.h.f'
	real*8 eta,emaxi,timei,al,au,epsr,epsa
	integer key,neval,ier,limit,lenw,last
	real*8 pbhole
	real*8 funct,res,abser,xmaxi,pmodel
	parameter(limit=1000, lenw=limit*4)
	real*8 work(lenw)
	integer iwork(limit)
	common /turnpt/ emaxi,timei  
	external pmodel,dQAG, funct,pbhole

	emaxi=pmodel(xmaxi,0.d0,0.d0)+pbhole(xmaxi,0.d0,0.d0)+const

	al = 0.d0
	au = xmaxi 
	epsr = 1.d-5
	epsa = 1.d-5
	ier=6
	key=6
	neval=0
	last=0
	call dQAG(funct,al,au,epsa,epsr,key,res,abser,neval,ier,
     &            limit,lenw,last,iwork,work)
	if (ier .ne. 0) write (*,*) 'Fail in per, ier,nevl=',ier,neval
	timei=2.d0*dsqrt(2.d0)*res
	
	return
	end

c----------------------------------------------------------------------------c
	function funct(x)
	include 'model.h.f'
	real*8 funct,x,emaxi,timei
	real*8 pmodel,v,pbhole
	common /turnpt/ emaxi,timei
	external pmodel, pbhole

	v=pmodel(x,0.d0,0.d0)+pbhole(x,0.d0,0.d0)+const
	
	funct=1.d0/(dsqrt(emaxi-v))
	return
	end
	
c-------------- -------------------------------------------------------------c

