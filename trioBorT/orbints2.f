c modified Feb 2017, to use dop853_09.f (version of 2009 from web)
c some array variables redefined
c--------------------------------------------------------------
c
      subroutine orbint(x)
c
c--------------------------------------------------------------
c
c Uses the version Dop853.f which provides output at a dense number of points
c via interpolation
c This routine also adds forces due to a Black Hole (provided by fbhole.f)
c This routine also adds centrifugala nd coriolis forces due to Figure rotation
c  of the triaxial figure.
      implicit none
      include 'trio.h.f'
      include 'model.h.f'
      include 'fmarray.f'
      integer ipar(6), i, nc,nstpmx,istate,istat,nj,j,itol
      INTEGER NR,IDID,NRDS
      integer NDGL, LWORK, LIWORK, NRD
c    &   , LRCONT, LICONT
      PARAMETER (NDGL=6,NRD=NDGL,LWORK=11*NDGL+8*NRD+21,LIWORK=NRD+21)
c      PARAMETER (LRCONT=8*NRD+2,LICONT=NRD+1)
      real*8 x(nvar),tn,tol,tnext,tt, eta
      real*8 rpar(6), pmodel,eng0,engJ
c     variables for dop853:

      INTEGER NFCN, NSTEP, NACCPT, NREJCT, IOUT, IWORK(LIWORK)
c    &  , ICONT(LICONT)
      REAL*8 WORK(LWORK), XOLD, HOUT
c    &, RCONT(LRCONT)

      real*8 ATOL(1), RTOL(1)
      real*8 dt

c-------------------------
      external SOLOUT,der1,fmodel,pmodel,dop853
      common /energy/ eng0, engJ
      common /state/ istat
      COMMON/STATD8/NFCN,NSTEP,NACCPT,NREJCT
c      COMMON /COD8R/XOLD,HOUT,RCONT
c      COMMON /COD8I/NRDS,ICONT
c      COMMON /COD8R/XOLD,HOUT,RCONT(LRCONT)
c      COMMON /COD8I/NRDS,ICONT(LICONT)
      common /interv/ dt,nj

      ndat = nbmax
      eta = 3.-gamma

c---------------------------------------------------------------
      do j=1,nvar
         tout(j) = 0.d0
      end do

c Start integration: 
      tn = 0.d0
      dt = (tmax - tn)/dfloat(nbmax)

      if(istat.eq.0) then
c      open(9,file=trim(freqfile)//'.out'
c     &  ,status='old',access='append')
c         write(9,*) 'Total integration time for this orbit', tmax
c         write(9,*) 'orbit integration timestep = ', dt
c         write(9,*) 'nbmax', nbmax
c         close(9)
      end if
C --- REQUIRED (RELATIVE) TOLERANCE
      tol = 1.d-10
      ITOL=0
      RTOL=TOL
      ATOL=TOL
C --- DEFAULT VALUES FOR PARAMETERS
      DO  I=1,10
        IWORK(I)=0
        WORK(I)=0.D0
      end do
      IWORK(5)=NVAR
      IWORK(4)=1
c work(5) "is "beta" for stabilized step control. +ve values < 0.04 make step
c size control more stable
      work(5) = 0.01
c
      tnext = tmax

      nj = 1
C --- CALL OF THE SUBROUTINE DOPRI853:   

      iout = 2

c      write(*,*) 'just before dop853, x ', (x(j), j=1,6)

c        CALL DOP853(NVAR,der1,tn,x,tnext,
c     &                  RTOL,ATOL,ITOL,
c     &                  SOLOUT,IOUT,
c     &                  WORK,LWORK,IWORK,LIWORK,LRCONT,LICONT,IDID)

c Der1= FCN

        CALL DOP853(NVAR,der1,tn,x,tnext,
     &                  RTOL,ATOL,ITOL,
     &                  SOLOUT,IOUT,
     &                  WORK,LWORK,IWORK,LIWORK,RPAR,IPAR,IDID)
        
c---------------------- end of routine orbint ------------------
        return

        end
c--------------------------------------------------------------
      SUBROUTINE SOLOUT (NR,XOLD,x,y,Nvariable,CON, ICOMP, ND,
     c                   RPAR, IPAR, IRTRN,XOUT)
C --- PRINTS SOLUTION AT EQUIDISTANT OUTPUT-POINTS
C --- BY USING "CONTD8", THE CONTINUOUS COLLOCATION SOLUTION
      implicit none
      include 'trio.h.f'
      include 'model.h.f'
      include 'fmarray.f'
      integer i,nj,j, NVariable,NR, IRTRN, ND, ipar(*)
      REAL*8 XOLD,X,Y(Nvar),CON(8*ND)
      INTEGER ICOMP(ND)
      real*8 dt, xout, rpar(*)
      REAL*8  CONTD8, pp, qq
      common /interv/ dt,nj
c      COMMON /INTERN/ XOUT
      external contd8

       
      IF (NR.EQ.1) THEN
c           WRITE (*,99) x,y(1),y(2),NR-1

         if(nj.le.nbmax) then
            tout(nj) = x
            fkx(nj) = cmplx(y(1),y(4))
            fky(nj) = cmplx(y(2),y(5))
            fkz(nj) = cmplx(y(3),y(6))
         end if
         nj = nj+1
         XOUT = X + dt
        ELSE
 10        CONTINUE
           IF (X.GE.XOUT) THEN
c              WRITE (*,99) XOUT,CONTD8(1,XOUT,CON,ICOMP,ND),
c     c                      CONTD8(2,XOUT,CON,ICOMP,ND),NR-1

           if(nj.le.nbmax) then
              tout(nj) = xout
              pp = contd8(1,xout,CON,ICOMP,ND)
              qq = contd8(4,xout,CON,ICOMP,ND)
              fkx(nj) = cmplx(pp,qq)
              pp = contd8(2,xout,CON,ICOMP,ND)
              qq = contd8(5,xout,CON,ICOMP,ND)
              fky(nj) = cmplx(pp,qq)
              pp = contd8(3,xout,CON,ICOMP,ND)
              qq = contd8(6,xout,CON,ICOMP,ND)
              fkz(nj) = cmplx(pp,qq)

           end if
           nj = nj+1
           XOUT=XOUT+DT
           GOTO 10
           END IF
        END IF
c 99     FORMAT(1X,'T =',5f.2,'    X =',2E18.10,'    NSTEP =',I4)
        RETURN
        END

***********************************************************************
      SUBROUTINE der1(nc,t,xx,dxxdt,rpar, ipar)
***********************************************************************
*     PURPOSE:
*       Compute the acceleration in triaxial potential.
*     ARGUMENTS:
*       Input
*       -----
*       nc      number of coordinates
*       t       time
*       xx      stellar coordinates (x,y,z,vx,vy,vz)
*
*       Output
*       ------
*       dxxdt   time derivative of elements of xx
*     ROUTINES CALLED:
*       fmodel  routine to compute force & force derivatives
*              and forces due to rotation (if required)
***********************************************************************
      implicit none
      include 'trio.h.f'
      include 'model.h.f'
      include 'fmarray.f'
      INTEGER nc
      real*8 t,xx(nc),dxxdt(nc)
      INTEGER iliap
      external fmodel,fbhole
      integer ipar(*)
      real*8  x,y,z,fx,fy,fz,fxb,fyb,fzb
      real*8 rpar(*)
*----------------------------------------------------------------------
*       Set velocity 
*----------------------------------------------------------------------
      dxxdt(1) = xx(4)
      dxxdt(2) = xx(5)
      dxxdt(3) = xx(6)
*----------------------------------------------------------------------
*       Compute acceleration 
*----------------------------------------------------------------------
      x = xx(1)
      y = xx(2)
      z = xx(3)
      CALL fmodel (x,y,z,fx,fy,fz)
      call fbhole (x,y,z,fxb,fyb,fzb)
c The equations below were checed on Dec 29, 2014 and compared with BT08
c equations 3.116 and 3.117 and found to be correct
      if(yrot) then
         dxxdt(4) = fx + fxb +2.d0*(omega*xx(5))+omega**2*xx(1)
         dxxdt(5) = fy + fyb -2.d0*(omega*xx(4))+omega**2*xx(2)
         dxxdt(6) = fz + fzb
      else
         dxxdt(4) = fx + fxb
         dxxdt(5) = fy + fyb
         dxxdt(6) = fz + fzb
      end if
      RETURN
      END
