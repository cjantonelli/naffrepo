c kernel density routine with upper and lower limits specified
c ---------------------------------------------------------------------c
c
      subroutine kerneldens(N,Xin,xmin, xmax,nker,xx,yker)
c
c ---------------------------------------------------------------------c
      implicit none
      integer N, nker, i, k, j
      real*4 Xin(N),xmin,xmax,xx(Nker),yker(nker)
      real*4 dx, dh, kernel, const, sum, term, frac, dy
c If you want to normalize kernels to unity:      
      dy = 1./float(N)
c if you want to normalize kernels to N use kerneldens_n_lims or set dy=1
      dx = abs((xmax-xmin)/float(nker-1))
      dh = 3.5*dx
      const = (15.*dy)/(16.0*dh)
      write(*,*) 'xmin, xmax, dh', xmin, xmax, dh
      do j = 1,nker
         xx(j) = xmin + float(j-1)*dx
         yker(j) = 0.
      end do
      sum = 0.

      do j = 1,nker
         do i = 1,N
            frac = abs((xin(i)-xx(j))/dh)
            if(frac.le.1.) then
               term = kernel(frac)
               yker(j) = yker(j) + const*term
            end if
c               write(173,*) j, yker(j)
         end do
      end do
      return
      end

c---------------------------------------------------------------------c
c
      real function kernel(t)
c
c---------------------------------------------------------------------c
c Biweight Kernel:
      real*4 t
      kernel = (1.d0-t**2)**2
      return
      end

c---------------------------------------------------------------------c
c      include '../progs/minmax.f'
