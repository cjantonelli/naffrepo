c     (c) Monica Valluri  university of michigan
c       
c     original version June 30 2008:
c modified Aug 2011 to compute drift associated with largest amplitude
c modified Sept 2014 to use orbits with rapo < 150kpc rather than nperiod >20
      program chaos_stats
C    a) dft = abs[(f(T1) - f(T2))/f(T1)] where f is the freq associated with
c        largest amplitude in phase 1
c    b) histogram of dft

      implicit none
      integer i, j, k, m,n, npart,imax,nphase,nhist,ntime,npartmax
      parameter (npartmax=50000, ntime=2,nphase=1,nhist=100)
c here phase is the section of the timeseries: 
c   timeseries 1 = 1, ntstep/2
c   timeseries 2 = ntstep/2+1, ntstep
      character*80 linestring
      integer partno(npartmax),ipart,ip, nplt,ntmp
      real*4 lff(npartmax,ntime,nphase), omega(3), ampl(3), meandf,imean
      real*4 adev, sdev, var, skew,curt, median
      real*4 xpl(npartmax), ypl(npartmax)
      real*4 xmin, xmax, ymin, ymax,ejunk,ampmax
      real*4 nper(npartmax), rapo(npartmax), rperi, rcmin(npartmax),
     &       rcap(npartmax), lmean(3,npartmax), lmax(3,npartmax),
     &       Jmean(2,npartmax),Jmx(2,npartmax), Jnt(2,npartmax),
     &       etot(npartmax)
      real*4 yker(nhist),xx(nhist),xdif(npartmax), ydif(npartmax)
      real*4 delx
      real*4 delh, cdfy(nhist+1), cdfx(nhist+1), dy
      character*2 tset
      character*7 infile
      character*50 label

      write(*,*) 'input file name prefix (a7)? '
      read(*,*) infile
      open(8, file = infile//'_2T.dat', status='old')
      write(*,*) 'give text label (a50) for figs'
      read(*,'(a50)') label
      write(*,*) 'number of particles in file?'
      read(*,*) npart
      do n = 1,nphase
      read(8,'(a)') linestring
      do i = 1,npart
         lff(i,1,n) = -10000000.
         read(8,*) ipart, tset,(omega(j),j=1,3),(ampl(j),j=1,3),
     & ejunk
c      Find largest amplitude:
         imax=1
         ampmax=ampl(1)
         if(ampl(1).lt.ampl(2)) then
            imax=2
            ampmax=ampl(2)
         end if

         if(ampmax.lt.ampl(3)) then
            imax=3
            ampmax=ampl(3)
         end if
         
c         write(8,*) ipart, tset,(omega(j),j=1,3),(ampl(j),j=1,3),
c     & ejunk
         if(ipart.ne.i) write(*,*) 'read error', ipart,i
         lff(i,1,n) = abs(omega(imax))
         read(8,*) ipart,tset, (omega(j),j=1,3),(ampl(j),j=1,3),
     & ejunk
         lff(i,2,n) = abs(omega(imax))
      end do
c      read(8,'(a)') linestring
      end do
c done reading file:
      close(8)

c      Read file name with orbit info (***_AO.dat)?
      open(9,file=infile//'_AO.dat', status='old')
      read(9,'(a)') linestring
      do i = 1,npart
         read(9,*) k,nper(i),rapo(i),rperi,rcmin(i),rcap(i),lmean(1,i),
     &   lmax(1,i),lmean(2,i),lmax(2,i),lmean(3,i),lmax(3,i),
     &   (Jmean(j,i),j=1,2),(Jmx(j,i),j=1,2),
     &   (Jnt(j,i),j=1,2),etot(i)
      end do

c Done reading datafile
c Now plotting:
      open(12,file=infile//'_drif.dat',status='unknown')
      do n = 1,nphase
         do i = 1,npart
               ip=ip+1
               xpl(ip) = lff(i,1,n)
               ypl(ip) = abs((lff(i,1,n) - lff(i,2,n))/lff(i,1,n))
               write(12,*) i, lff(i,1,n), ypl(ip)
         end do
         write(12,*) '          '

c     Plot various things:
c min and max of ft, fd:
      ip = 0
      do i = 1,npart
            if(rapo(i).le.100.) then
               ip=ip+1
               xdif(ip) = xpl(i)
               if(ypl(i).ne.0.)then
                  ydif(ip) = log10(ypl(i))
               else
                  ydif(ip) = -7.
               end if
            end if
      end do

      nplt= ip
      open(10,file=infile//'_difhistN.dat',status='unknown')
      call moment(ydif, nplt, meandf, adev, sdev, var, skew,curt)
c----------------------------------------------------------------c
      write(10,*) '--------------------------------------------'
      write(10,*) 'mean(df) = ', meandf, 'sigma =', sdev
      write(10,*) 'skew, curt', skew, curt
      write(10,*) 'median ', median(npart,ydif)
      write(10,*) '--------------------------------------------'
      call minmax(nplt,ydif,xmin,xmax)
      write(*,*) 'min and max diffusion', xmin, xmax
      write(*,*) 'give min max diffusion ', xmin, xmax
      read(*,*) xmin, xmax
      call kerneldens(nplt,ydif,xmin,xmax,nhist,xx,yker)      
      call minmax(nhist,yker,ymin,ymax)

      write(10,*) nhist
      do i = 1,nhist
         write(10,*) i, xx(i), yker(i)
      end do
      close(10)
      call pgbegin(0,trim(infile)//'_diff.ps/vps',1,1)
      call pgsch(1.2)
      call pgslw(4)
      call pgwindow(xmin, xmax, ymin, ymax*1.1)
      call pgvsize(1.,5., 2.0, 5.0)
      call pgbox('bcnst', 1., 1, 'bcst', 2000., 2)
      call pglabel('log\d10\u(\gDf)', ' ',' ')
      call pgslw(4)
      call pgline(nhist, xx, yker)
      call pgtext(-4.5,ymax*.95,label)
c      call legend(3,0.1,ymax*.7,.2,'  ')
c end of loop over nphase
      end do
      call pgend
c-------------------------------------------------------------------c     
      stop
      end

c--------------------------------------------------------------------c
C From Numerical Recipes:
      SUBROUTINE moment(data,n,ave,adev,sdev,var,skew,curt)
      INTEGER n
      REAL adev,ave,curt,sdev,skew,var,data(n)
      INTEGER j
      REAL p,s,ep

      write(*,*) 'inside moment'
      if(n.le.1) stop'n must be at least 2 in moment'
      s=0.
      do 11 j=1,n
        s=s+data(j)
11    continue
      ave=s/n
      adev=0.
      var=0.
      skew=0.
      curt=0.
      ep=0.
      do 12 j=1,n
        s=data(j)-ave
        ep=ep+s
        adev=adev+abs(s)
        p=s*s
        var=var+p
        p=p*s
        skew=skew+p
        p=p*s
        curt=curt+p
12    continue
      adev=adev/n
      var=(var-ep**2/n)/(n-1)
      sdev=sqrt(var)
      if(var.ne.0.)then
        skew=skew/(n*sdev**3)
        curt=curt/(n*var**2)-3.
      else
        write(*,*) 'no skew or kurtosis when zero variance in moment'
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software *!^3#!0Y..

c      include 'sort_ascend.f'
c      include 'legend.f'
c      include 'kerneldens_lims.f'
c      include 'minmax.f'
c      include 'median.f'

