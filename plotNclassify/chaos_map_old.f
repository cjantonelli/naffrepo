c     (c) Monica Valluri, University of Michigan
c     * use output of frequency analysis files (xxxxxxx_ffrq.dat, xxxxxxx_AO.dat) to make a Cartesian frequency map
c       : plot wy/wx versus wx/wz for all orbits
c     *  color particles by their energies (in 3 bins)
c     * overplot some commonly occuring "resonance line" with labels
c       (note: SAT, LAT lines are not true resonances)
c   this version only plots points for orbits with np> 20
c     written Jan 2009
c     last edited May 2019
c********************************************
      program chaos_maps_E
      implicit none
      integer i, j, k, m,n, ir, npart, nphase, ichao, itmp,nres,
     & npartmax
      parameter (npartmax=50000, nphase=1, nres=9)
c here phase is the section of the timeseries: 
c   timeseries 1 = 1, ntstep/2
c   timeseries 2 = ntstep/2+1, ntstep
      character*80 linestring
      integer ipart, nplt, mphase, onethird, iplt, jpart
      real*4 omega(3), ampl(3), meandf,imean
      real*4 adev, sdev, var, skew,curt, x1,x2,y1,y2, etmp
      real*4 xplt(npartmax), yplt(npartmax), nptmp
      real*4 ff(npartmax,nphase), fdrift(npartmax)
      real*4 xmin, xmax, ymin, ymax,ejunk, emin, emax, delE
      integer ntmp,mtmp,np, indx(npartmax)
      real*4 nper(npartmax), rapo(npartmax), rperi, rcmin(npartmax),
     &       rcap(npartmax), lmean(3,npartmax), lmax(3,npartmax),
     &       Jmean(2,npartmax),Jmx(2,npartmax), Jnt(2,npartmax),
     &       etot(npartmax)
      real*4 fratioy(npartmax, nphase), fratiox(npartmax, nphase)
      real*4 ebin(npartmax), fratiod(npartmax, nphase), omega2(3)
      real*4 oxbyoz(npartmax,3),oybyoz(npartmax,3),eplot(npartmax)
      real*4 term1, term2, term3, fmin, fmax, flim
      character*2 tset
      character*16 xtube, ztube
      character*7 infile
      character*40 figlab
      character*7 reslabel(nres)
      real*4 labx(nres), laby(nres), labangle(nres)
      real*4 resm(nres), resc(nres), drx
      real*4 resx(10), resy(10)

      xtube=' long-axis tubes'
      ztube='short-axis tubes'
c resonace labels:
      reslabel(1)= '0,1,-1 '
      reslabel(2)= '1,-1,0 '
      reslabel(3)= '2,0,-1 '
      reslabel(4)= '3,0,-2 '
      reslabel(5)= '3,-1,-1'
      reslabel(6)= '2,1,-2 '
      reslabel(7)= '2,-1,0 '
      reslabel(8)= '3,-2,0 '
      reslabel(9)= '-1,2,-1'
c locations of resonance labels:
      labx(1) = 1.05
      laby(1) = 1.01
      labangle(1) = 0.
      labx(2) = 1.05
      laby(2) = 1.06
      labangle(2) = 45.
      labx(3) = 0.49
      laby(3) = 1.05
      labangle(3) = 90.
      labx(4) = 0.65
      laby(4) = 1.05
      labangle(4) = 90.
      labx(5) =0.0 
      laby(5) = 0.0
      labangle(5) = 71.56
      labx(6) = 0.74
      laby(6) = 0.55
      labangle(6) = -63.44

      labx(7) = 0.4
      laby(7) = 0.81
      labangle(7) = 63.44


      labx(8) = 0.4
      laby(8) = 0.61
      labangle(8) = 56.31

      labx(9) = 0.7
      laby(9) = 0.86
      labangle(9) = 26.56
c 
c slope and intercept of resonance lines:
      resm(1) = 0.
      resc(1) = 1.
      resm(2) = 1.
      resc(2) = 0.
      resm(5) = 0
      resc(5) = -0.01
      resm(6) = -2.
      resc(6) =  2.
      resm(7) =  2.
      resc(7) =  0.
      resm(8) = 1.5
      resc(8) =  0.
      resm(9) = 0.5
      resc(9) =  .5
      
c  read input data 
      write(*,*) 'give input file name'
      read(*,*) infile
      write(*,*) ' Give label (a20)'
      read(*,'(a40)') figlab

      write(*,*) 'number of orbits ?'
      read(*,*) npart

c      Read file name with orbit info (***_AO5.dat)?
      open(9,file=infile//'_AO.dat', status='old')
      read(9,'(a)') linestring
      do i = 1,npart
         read(9,*) k,nper(i),rapo(i),rperi,rcmin(i),rcap(i),lmean(1,i),
     &   lmax(1,i),lmean(2,i),lmax(2,i),lmean(3,i),lmax(3,i),
     &   (Jmean(j,i),j=1,2),(Jmx(j,i),j=1,2),
     &   (Jnt(j,i),j=1,2),etot(i)
      end do
      
c     Read file with frequency info (*******_ffrq.dat)
      open(8, file = infile//'_ffrq.dat', status='old')
      nplt= 0
      iplt=0
      do n = 1,nphase
      read(8,'(a)') linestring
      do i = 1,npart
         read(8,*) ipart,(omega(j),j=1,3),(ampl(j),j=1,3), etot(i)
         IF(abs(omega(3)).eq.1.e-12) omega(3) = -1.e-3
c        checking period of orbit > 20
c         if(nperiod(i).ge.20.) then
            iplt = iplt+1
            fratiox(iplt,n) = abs(omega(1)/omega(3))
            fratioy(iplt,n) = abs(omega(2)/omega(3))
c            write(*,*) 'fxz, fyz' , fratiox(iplt,n), fratioy(iplt,n)
            eplot(iplt) = etot(i)
c         end if
      end do
      nplt= iplt
      read(8,'(a)') linestring
      end do
      write(*,*) 'number of orbits', nplt
c done reading file:
      close(8)

c sort particles in energy
      call indexx(nplt, eplot, indx)

c Plot frequency map
      call pgbegin(0,infile//'_mapE.ps/vcps',1, 1)
      call pgsch(1.1)
      call pgslw(3)
      do n = 1,1
         call pgsci(1)
        call pgsch(1.)
         call pgslw(5)
         call pgvsize(1.5, 6.5, 1.5, 6.5)
         write(*,*) 'give axes limits for plot: x1, x2, y1, y2'
         write(*,*) ' e.g. 0.3 1.2 0.3 1.2'
         read(*,*) x1,x2,y1,y2
         call pgwindow(x1,x2,y1,y2)
         call pgbox('bncst', 0.2, 2, 'bcnst', 0.2, 2)
         call pgsch(1.4)
         call pgptxt(x2*0.9, y1*1.1,1.,1., figlab)
         call pglabel('\gW\dx\u/\gW\dz\u', 
     & '\gW\dy\u/\gW\dz\u', ' ')

c 3 Energy/Ej bins with equal number of particles in each bin
      onethird = int((nplt)/3)
      write(*,*) 'one third of orbits is', onethird
      call pgsci(4)
      call pgsch(0.5)
      np=0
      do i = 1, onethird
         np=np+1
         xplt(np) = fratiox(indx(i),n)
         yplt(np) = fratioy(indx(i),n)
      end do
c if using lots of orbits uncomment the following line:
c      call pgpt(np, xplt, yplt,1)
c for an example with only a few points use this:

      call pgpt(np, xplt, yplt,24)

      np = 0
      call pgsci(3)
      do i = onethird+1, onethird*2
         np = np+1
         xplt(np) = fratiox(indx(i),n)
         yplt(np) = fratioy(indx(i),n)

      end do
c if using lots of orbits uncomment the following line:
c         call pgpt(np, xplt, yplt,1)
c for an example with only a few points use this:
      call pgpt(np, xplt, yplt,24)

      np = 0
      call pgsci(2)
      do i = onethird*2+1, nplt
         np = np+1
         xplt(np) = fratiox(indx(i),n)
         yplt(np) = fratioy(indx(i),n)
      end do
c if using lots of orbits uncomment the following line:
c       call pgpt(np, xplt, yplt,1)
c for an example with only a few points use this:
      call pgpt(np, xplt, yplt,24)
      call pgsci(1)
      call pgsch(1.)

c now plot nres resonance lines:
      drx = (1.2-0.3)/8.
      write(*,*) 'plotting only 4 resonances'
      do ir = 1,4
         if(ir.ne.3 .and. ir.ne.4) then
            do m = 1,10
               resx(m) = 0.3 + drx*float(m-1)
               resy(m) =resm(ir)*resx(m)+resc(ir)
            end do
         else
            if(ir.eq.3) resx(1) = 0.5
            if(ir.eq.4) resx(1) = 0.66667
            resy(1)= 0.3
            do m = 2,10
               resx(m) = resx(1)
               resy(m) = 0.3 +drx*float(m-1)
            end do
         end if

         call pgsls(2)
         call pgslw(1)
         call pgline(10, resx, resy)
         call pgslw(2)
         call pgptext(labx(ir),laby(ir),labangle(ir),0.,reslabel(ir))
         call pgptext(0.35,0.35,45.,0.,ztube)
         call pgptext(0.7,1.02,0.,0.,xtube)
      end do

      end do
      call pgend
c-------------------------------------------------------------------c     

      stop
      end

c      include 'INDEXX.f'
c      include 'minmax.f'
