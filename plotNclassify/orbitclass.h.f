c orbitclass.h.f
c common arrays for orbitclass.f
      integer i,j,n,k,l,m,npart,nvar,nratio,ipart,nphase,npartmax
      real*4 difflim
c********************************************************\\
c YOU CAN CHANGE THESE next two parameters
      parameter (npartmax=50000)
      parameter (difflim=-1.2)
c Do not change anything below unless you absolutely know what you are
c doing!
      parameter (nvar=3,nphase=1, nratio=3)
      integer partno(npartmax),mphase, nbin
      real*4 ff(npartmax,nphase,nvar),ampl(npartmax,nphase,nvar)
      integer ratio(npartmax,nphase,nratio,2)
      real*4 r(nratio), den(nratio), num(nratio)
      character*4 orbtype(npartmax,nphase)
      character*14 resonance(npartmax,nphase)     !nnnn:mmmm:kkkk
      integer resonint(npartmax, nphase,NRATIO)
      character*7 infile
      logical rational(npartmax, nphase, nratio)
      common /ratios/ i,k, m,r, rational, ratio, den, num
      common /reson/ resonance, resonint
