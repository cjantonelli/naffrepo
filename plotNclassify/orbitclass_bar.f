c     (c) Monica Valluri, University of Michigan
c program to classify orbits as box/tube/ chaotic  based on properties of
c     frequency spectrum
c this version tuned for orbit classification in bars
c     Version of Nov 23, 2012: major modification to allow for IATs
c version of June 30, 2008: minor modification with reading files
      program orbitclass
      implicit none

      include 'orbitclass.h.f'
      real*4 omr(npartmax), omphi(npartmax), omz(npartmax),ej0(npartmax)
      real*4 ammr(npartmax), ammphi(npartmax), ammz(npartmax)
      real*4 lff(npartmax),diff(npartmax), reslim1,reslim2
      real*4 Jxav(npartmax),Jzav(npartmax),Jxmx(npartmax),Jzmx(npartmax)
      real*4 xm,xmax(npartmax),ymax(npartmax),zmax(npartmax)
     &    ,rmax(npartmax),barlen, Jxnt(npartmax),Jznt(npartmax)
      real*4 xav(npartmax),yav(npartmax),zav(npartmax),ymx
      real*4 rperi(npartmax),rapo(npartmax),tmp, nper, rdisk(npartmax)
      real*4 rchao(npartmax), rcapo(npartmax), rcperi(npartmax)
      integer junk, ndisk, nchao
      character*80 linestring
      character*4 typeca(npartmax),chaoreg(npartmax,1)
      external rationalize
      external lcdclass
c First Triaxial orbit types:
c perd - periodic  (m:n:l)
c xtub - wz = wy or den(2)=num(2)
c obox - open box (one resonance or two high order resonance)

      write(*,*) 'input file name prefix? (a7)'
      read(*,*) infile
      write(*,*) 'input file is', infile
      open(8, file = infile//'_ffrq.dat', status='old')
      write(*,*) 'number of particles?'
      read(*,*) npart
      do k = 1, 1
      read(8,'(a)') linestring
         do i = 1,npart
            do j=1,nvar
               ff(i,k,j) = -1000000.
            end do
            orbtype(i,k) = '   '
            read(8,*) partno(i),(ff(i,k,j),j=1,3),
     &                (ampl(i,k,j),j=1,3)
         end do
         read(8,'(a)') linestring
      end do
      close(8)

      open(8, file = infile//'_AO.dat', status='old')
      read(8,'(a)') linestring
      do i = 1,npart
         read(8,*) junk,nper,rapo(i),rperi(i),rcapo(i), rcperi(i),
     &    xav(i),xmax(i),yav(i),
     &    ymax(i),zav(i),zmax(i),Jxav(i),Jzav(i),Jxmx(i),Jzmx(i),
     &    Jxnt(i),Jznt(i),Ej0(i)
      end do
      close(8)
c using Diffusion rates from Method 3: difference of largest amplitude FF:
c from chaos_stats.f:

      open(12,file=infile//'_drif.dat',status='old')
      do i = 1,npart
           read(12,*) junk, lff(i), diff(i)
           if(diff(i).ne.0.) then
              diff(i) = log10(diff(i))
           else
              diff(i) = -7.
           end if
c Now set everything with diffusion lim >difflim = chaotic
           if(diff(i).gt.difflim) then
                   chaoreg(i,1)='chao'
           else
                   chaoreg(i,1)='regl'
           end if
      end do
      close(12)
c---------------------------------------------------------------------c
      reslim1 = 0.01
      reslim2 = 0.001
      write(*,*) 'give barlength (in prog units; 0 if no bar )'
      read(*,*) barlen
      if(barlen.eq.0.) barlen=1000000.
      
      write(*,*) 'using diffusion limit for chaotic orbits of', difflim
      write(*,*) 'using resonance criterion for Ophi/Oz=1 of', reslim1
      write(*,*) 'using resonance criterion for oR/ophi=2 of', reslim2
      write(*,*) 'assuming barlength ~', barlen
c      open(9,file=infile//'_clsbar.dat', status='unknown')
      
c      write(9,*) '     Orb#  wy:wx  wz:wy  wz:wx '
      open(10,file=infile//'_typbar.dat', status='unknown')
      open(11,file=infile//'_periodic.dat', status='unknown')
c     
      do i = 1, npart
      do k = 1, 1
c default assumption - orbit is regular
         typeca(i)='regl'
c initialize ratio: last two array elements of 
c   ratio(npart, nphase, nratio, nd), nd(1)= num, nd(2)=den
c if num = den=0 => ratio is irrational or at least cannot
c be described as a ratio of two numbers with den.le.100.
           do m = 1,nratio
              ratio(i,k,m,1) = 0
   	      ratio(i,k,m,2) = 0
	   end do
c determine ratios of base frequency
	   if(ff(i,k,1).ne.0.) then
	     r(1) = abs(ff(i,k,2)/ff(i,k,1))
	     r(3) = abs(ff(i,k,3)/ff(i,k,1))
   	   else
		r(1) = 1000.
		r(3) = 1000.
	   end if
 	   if(ff(i,k,2).ne.0.) then
              r(2) = abs(ff(i,k,3)/ff(i,k,2))
	   else
              r(2) = 1000.
	   end if

c call subroutine to rationalize the ratios:
           call rationalize
c Find LDC when frequency ratios are rational:
c orbit types:
c pbox - periodic box (m:n:l)
c pzlp - periodic z-loop (m:m:l) 
c pxlp - periodic x-loop (l:m:m) 
c ztub - wy = wx or num(1)=den(1)
c xtub - wz = wy or den(2)=num(2)
c obox - open box (one resonance or two high order resonance)
c	 'wy/wx=',ratio(i,k,1,1),'/',ratio(i,k,1,2),
c     &	 'wz/wy=',ratio(i,k,2,1),'/',ratio(i,k,2,2),
c     &	 'wz/wx=',ratio(i,k,3,1),'/',ratio(i,k,3,2)

c Condition  rperi>barlen to select disk star:
        if(rapo(i).gt.barlen) then
           orbtype(i,k) ='disk'
        else
c c default orbit type is:
           orbtype(i,k) ='qqqq'
        call periodic
        if(rational(i,k,1).and.rational(i,k,2).and.rational(i,k,3)) 
     &     then
c start by assuming periodic box:
          orbtype(i,k)='boxp'          
           if(int(num(1)).eq.3.and.int(den(1)).eq.2) 
     &                         orbtype(i,k) = 'bo32'
           IF(abs(Jznt(i)).gt.0.9) orbtype(i,k)='ztup'
           IF(abs(Jxnt(i)).gt.0.9.and.int(num(2)).eq.int(den(2))) 
     &     orbtype(i,k)='xtup'
           if(int(num(3)).eq.5.and.int(den(3)).eq.3) 
     &                orbtype(i,k)='bobr'
        else
c First separate xtube orbits: 
           if(abs(Jxnt(i)).gt.0.8.or.num(2).eq.den(2)) then
                orbtype(i,k)='xtub'
           else 
              if (abs(Jznt(i)).gt.0.9) then
c could be x4, x1,x2 or ztub
                 if(Jznt(i).lt.0.) then 
                    orbtype(i,k)='x4++'
c if Jznet <0, x4 
                 else
                    if(ymax(i)/xmax(i).gt.1.2) then
c if elongated along y it is x2
                       orbtype(i,k) = 'x2++'
                    else
c it is  x1 or ztub
                       if(ymax(i)/xmax(i).lt.0.25) then
                          orbtype(i,k) = 'x1++'
                          if((int(num(1)).eq.6.and.int(den(1)).eq.2))  
     &                         orbtype(i,k) = 'x1cl'  
                          if(int(num(1)).eq.2.and.int(den(1)).eq.2) 
     &                         orbtype(i,k) = 'x122'
                          if(int(num(1)).eq.3.and.int(den(1)).eq.2) 
     &                         orbtype(i,k) = 'x132'
                          if(int(num(3)).eq.4.and.int(den(3)).eq.2)
     &                         orbtype(i,k) = 'x1bn'
                       else
                          orbtype(i,k) = 'ztub'
                       end if
                    end if
                 end if
c If you are here then you  are a box orbit (or disk orbit)
              else
                 orbtype(i,k) ='boxo'              
c some of these boxes are resonant so give them resonant classification
                 if(int(num(3)).eq.5.and.int(den(3)).eq.3) 
c                 if(ratio(i,k,3,1).eq.3.and.ratio(i,k,3,2).eq.5)
     &                orbtype(i,k)='bobr'
                 if(ratio(i,k,1,1).eq.3.and.ratio(i,k,1,2).eq.2)
     &                orbtype(i,k)='bo32'
c                 if(int(num(3)).eq.4.and.int(den(3)).eq.2)
                 if(ratio(i,k,3,1).eq.4.and.ratio(i,k,3,2).eq.2)
     &                orbtype(i,k) = 'bobn'

              end if
           end if
c     if(abs((omr(i)/ff(i,1,1))-2.).gt.0.1) orbtype(i,k) ='disk'
c           write(9,*) i, 'type: ', orbtype(i,k)
        end if
        end if
        write(10,*) i, '  ', orbtype(i,k), '   ', chaoreg(i,k)
        if(orbtype(i,k).eq.'qqqq') write(*,*)i,'is unclassified'
c     end of loop over nphase:	
      end do

c end of loop over npart:
      end do
c
      close(9)
      close(10)
      stop
      end
