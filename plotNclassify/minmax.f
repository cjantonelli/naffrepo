c subroutine to find min and max of an array of variables.

      subroutine minmax(ndat, x, xmin, xmax)
      integer ndat, i
      real*4 x(ndat), xmin, xmax

      xmin = 1.e+14
      xmax = -1.e+14
      do i = 1,ndat
         xmin = min(xmin,x(i))
         xmax = max(xmax,x(i))
      end do

      return
      end
