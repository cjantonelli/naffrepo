      subroutine periodic
c for 3 rational ratios find resonance
      include 'orbitclass.h.f'
      real*4  nprime(nratio), dprime(nratio)
      real*4 lcd, getlcd
      integer dd(nratio), nd(nratio)
      character*14 restmp
c      external getlcd
      restmp = '    :    :    '
c
c Find LCD for 3 rational ratios:

      if((den(1).eq.den(2)).and.(den(2).eq.den(3))) then
          write(restmp(1:4),'(i4)') int(num(2))
          write(restmp(6:9),'(i4)') int(num(1))
          write(restmp(11:14),'(i4)') int(num(3))
          resonint(i,k,1) = int(num(2))
          resonint(i,k,2) = int(num(1))
          resonint(i,k,3) = int(num(3))
          resonance(i,k) = restmp
          GOTO 102
      end if

      if((den(1).eq.den(2)).and.(den(2).ne.den(3))) 
     &     lcd = getlcd(den(1),den(3))
      if((den(1).eq.den(3)).and.(den(1).ne.den(2)))
     &	  lcd = getlcd(den(1),den(2))
      if((den(3).eq.den(2)).and.(den(3).ne.den(1))) 
     &    lcd = getlcd(den(3),den(1))

      if((den(1).ne.den(2)).and.(den(1).ne.den(3))) then
          lcd = getlcd(den(1),den(2))
          lcd = getlcd(den(3),lcd)
      end if

c tmp convertion of num/den to integers:
      do m = 1,nratio
          nd(m) = int(num(m))
          dd(m) = int(den(m))
      end do

  28  format(a6,2x,a6,i4,a1,i4,1x,a6,i4,a1,i4,1x,a6,i4,a1,i4,1x)
      do m=1,nratio
           nprime(m) = num(m)*(lcd/den(m))
           dprime(m) = lcd
           num(m) = nprime(m)
           den(m) = dprime(m)
      end do
c
c converting ratios to ones that have a common lcd should be done now:
c convert to integer:
      do m = 1,nratio
          nd(m) = int(num(m))
          dd(m) = int(den(m))
      end do

c      write(11,28)  'new ratios','n1/d1',nd(1),'/',dd(1),
c     &	 'n2/d2=',nd(2),'/',dd(2),
c     &	 'n3/d3=',nd(3),'/',dd(3)
      write(restmp(1:4),'(i4)') nd(2)
      write(restmp(6:9),'(i4)') nd(1)
      write(restmp(11:14),'(i4)') nd(3)
      resonint(i,k,1) = nd(2)
      resonint(i,k,2) = nd(1)
      resonint(i,k,3) = nd(3)
      RESONANCE(I,K) = RESTMP
c only use these if they are small integers because one can always
c multiply by arbitarily large numbers to get seemingly rational numbers
 102  if(resonint(i,k,1).le.10.and.resonint(1,k,2).le.10.and.
     & resonint(i,k,3).le.10) then
         write(11,*) i, 'is periodic', (rational(i,k,m), m=1,nratio)
         write(11,*) 'resonance:', resonance(i,k)
         write(11,*) 'resonance integers', (resonint(i,k,m),m=1,3)
      else
       do m = 1,nratio
          rational(i,k,m) = .false.
       end do
      end if

      return
      end

      function getlcd(d1, d2)
      integer i, j
      real*4 d1, d2
      integer dd1, dd2, llcd
c      
      dd1 = int(d1)
      dd2 = int(d2)
      do i = 1,dd2
         llcd = dd1*i
         if(mod(llcd, dd2).eq.0) then
            getlcd = float(llcd)
            go to 10
         end if
      end do
 10   continue
c     write(9,*) 'd1, d2', dd1, dd2, 'LCD', llcd
      return
      end

