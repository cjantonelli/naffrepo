c--------------------------------------------------------------------------c
      subroutine rationalize
c rationalize frequency ratios:
c--------------------------------------------------------------------------c
      include 'orbitclass.h.f'
      real*4 nu, nui, tmpres

c  Determine if ratios are rational:
c  if one of the frequency ratios is greater than 1000 then this 
c  is a planar orbit
c  < 1e-3 treat only the other two frequencies
        do m=1,nratio
           rational(i,k,m) = .false.
   	      if(r(m).lt.1000.) then
c	      do j = 2, 10
	      do j = 2, 5
	          num(m) = r(m)*float(j)
		  den(m) = float(j)
c        now check if numerator is less than 0.01 from an integer<20
		  nui = -1.
	   	  do l = 1, 19
                     if(abs(num(m)-float(l)).le.0.015) then
                        nui = float(l)
		        go to 100
		     end if
		  end do
 	      end do  
 100          continue
	      num(m) = nui
	      if(num(m).ne.-1.) then
                   rational(i ,k, m) = .true.
                   ratio(i, k, m, 1) = int(num(m))
                   ratio(i, k, m, 2) = int(den(m))
	      end if
c end of loop over nratio:
              end if
	 end do
c  	 write(9,*) i, k, (rational(i,k,m), m=1,nratio)
c	 write(9,18)  'ratios','wy/wx=',ratio(i,k,1,1),'/',ratio(i,k,1,2),
c     &	 'wz/wy=',ratio(i,k,2,1),'/',ratio(i,k,2,2),
c     &	 'wz/wx=',ratio(i,k,3,1),'/',ratio(i,k,3,2)
c         write(9,18) i, ratio(i,k,1,1),':',ratio(i,k,1,2)
c     &	 ,ratio(i,k,2,1),':',ratio(i,k,2,2)
c     &	 ,ratio(i,k,3,1),':',ratio(i,k,3,2)

c 18   format(i10,2x,i2,a1,i2,2x,i2,a1,i2,2x,i2,a1,i2)

      return
      end
