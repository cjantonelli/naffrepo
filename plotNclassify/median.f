      real*4 function median(n, x)
      implicit none
      integer n,i
      real*4 x(n)
      real*4 tmpx(n)
      integer indx(n)
      external indexx

      call indexx(n,x,indx)
      do i =1,n
         tmpx(i) =x(indx(i))
      end do

      if(mod(n,2).eq.0) then
         median = (tmpx(n/2)+tmpx(n/2+1))/2.
      else
         median = tmpx((n+1)/2)
      end if
      
      return
      end

c      include 'INDEXX.f'
