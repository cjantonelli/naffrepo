        subroutine ReadFile1(fn)
c Reads Orbits in single precision variables (x,y,z, vx, vy, vz)
c subroutine for reading in files
      implicit none
      integer sz,  i, j, k,nactual
      parameter( sz = 2000000)
      integer szf,ntstep
      real*4 x(sz), y(sz), z(sz)
      real*4 vx(sz), vy(sz), vz(sz)
      real*4 t(sz), potp, etot
      character*123 headerstr
      character*100 fn
      character*50 linestring
      common /steptim/ ntstep, t
      common /coordinates/ etot,x,y,z,vx,vy,vz

c     Readfile will read one single precision orbit
c     First line must have the total orbital energy Etot' 
c     Subsequent lines must have: t,x,y,z,vx,vy,vz

      open( unit=1,file=fn,status='old')
      read(1,*) etot
c      write(*,*) 'reading from file ',  fn, 'etot', etot
c First loop over timestep ndat
      ntstep=0
      do j = 1, sz
         ntstep = ntstep + 1
c         write(*,*) 'looping over timestep', j
         read(1,*,end=100,err=100) t(j),x(j),y(j),z(j),vx(j),vy(j),vz(j)
c         write(*,*) t(j),x(j),y(j),z(j),vx(j),vy(j),vz(j)
         if(j.gt.1) then
            if(t(j).le.t(j-1)) then
               write(*,*) 'error at timestep', j, t(j), t(j-1)
               stop
            end if
         end if
      end do
 100  continue
 10   close( 1 )
      ntstep = ntstep-1
c      write(*,*) 'actual timesteps read', ntstep
c done reading
	  return
      end 
c end of subroutine ReadFile
