Cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      Program FMAP
c
c This version computes frequencies in Cylindrical polar coordinates about z
c this works for short-axis tubes
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c version 4.1: version that allows each orbit to be in a separate file
c              and holds only one orbit in memory at a time. Also does
c              only one phase at at time. (Sept 2009)
c version 4.0a: version that allows each phase to have slightly different
c               number of time steps - to accomodate the 43M steps in PlA3
c Version 4.0: analyze a  single orbit from an N-body simulation (sept 2008)
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c OUTLINE for Version 4 (pared down)
c ----------
C 1)  read each orbit from datafile and create complex arrays
c 2)  For two successive time intervals call FUNDAFREQ:
c     a} Find frequencies and amplitudes of each orbit,(FRECODER)
c     b} Identify Fundamental frequencies (INTEGFREQ)
c     c} Calculate integers that correspond to each frequency (INTEGFREQ)
c 5)  OUTPUT FILES:
c     filename.out -- all inout data, freq, complex amplitudes for each comp
c     filename.frq -- frequencies and amplitudes
c     filename.flg -- flag file to terminate prog if necessary 
c read coordinate file in Cartesian coordinates                         
c but convert to polar cooridates (r, vR, theta, vTheta), z remains unchanged.  c Following Papaphillipou & Laskar:                                             
c  r= sqrt(x**2+y**2), theta=atan2(y,x)                                         
c  vR= (x*Vx + y*Vy)/r , vTheta= x*Vy - yVx                                     
c  fr = r+ ivR                                                                  
c ftheta= sqrt(2*vTheta)*(cos(theta)+ isin(theta)) 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c Variable declaration:
      implicit none 
      include '../naff_npg/fmarray.f'
      integer i,j,m,n,np,istat,tsl,irs,nflg, nfiletyp
      integer nvar, splot,npl, itmp, npart, ntstep, istart
      real*8 ti1,ti2
      real*8 nui(3), getcpu,time0,timen     
      real*8 x1tmp, x2tmp,psp1, psp2, psp3
      complex*16 aai(3)
c arrays for storing fundamental frequencies:
      integer partno(npartmax), nactualsteps
      parameter (nvar=6)    
      real x(nbmax), y(nbmax), z(nbmax)
      real vx(nbmax), vy(nbmax), vz(nbmax)
      real r(nbmax),vr(nbmax),th(nbmax),vth(nbmax)
      real etot(npartmax),ttmp(nbmax), etotal
      character*100 fileref
      CHARACTER*50 LINESTRING1, linestring2
      character*30 FileNames
      common /state/ istat
      common /timslice/ tsl
      common /steptim/ ntstep, ttmp
      common /coordinates/ etotal,x,y,z,vx,vy,vz
      external fundafreq, getcpu, ReadFile1

c--------------------------------------------------------------------------c
      write(*,*) 'File name for frequency output(a1..7) (one per phase)'
      read(*,'(a)') freqfile

      write(*,*) 'Number of particles?'
      read(*,*) npart
      IF(NPART.GT.NPARTMAX) STOP 'NPART too large, increase npartmax'

      istat = 0
      time0 = getcpu()
c-------------------------------------------------------------------------c
      write(*,*) 'give file with names of orbit files (with full path)'
      read(*,'(a)') FileNames
      write(*,*) 'reading orbit file names from ', Filenames

      open(21, file =FileNames,status='old')
      open(41, file=trim(freqfile)//'_ffcy.dat',status='unknown')
      write(41,102)'  N ', ' omega_R ','omega_phi',' omega_z ',
     &   ' A_R ','A_phi', ' A_z ','Etot '
 102   Format(1x,a4, 3(6x,a9,3x), 4(6x, a5 ,4x))
c File with nvec frequencies for each component  and corresponding integers:
       open(8,file=trim(freqfile)//'_cy.int',status='unknown')
       open(9,file=trim(freqfile)//'_cy.out' ,status='unknown')
       write(*,*) 'give orbit number to start at? (1 for new file)'
       read (*,*) istart

c Skip over particles if istart.ne.1:
       if(istart.ne.1) then
          do n = 1,istart-1
                read(21,'(a)') fileref
                write(*,*) 'skipping file ', n, fileref
         end do
      end if

c Loop over particles:
      do N = istart, NPART
         read(21,'(a)') fileref
         if(n.eq.1. or. mod(n,100).eq.0) write(*,*) 'n ', n, ' ',fileref
         open(2101, file=freqfile//'.flg',status='unknown')
         write(2101,*) n, 'reading from ',fileref 
c READ input orbit:
         call ReadFile1(fileref)
         etot(n) = etotal
         partno(n) = n
c         write(*,*) 'ntstep, nbmax', ntstep, nbmax
	 if(ntstep.gt.nbmax) stop 'increase nbmax in /naff_npg/fmarray.f'
	 do i = 1,ntstep
                tout(i) = dble(ttmp(i))
c convert to cylindrical polar coordiantes:
                call cart2pol(x(i),vx(i),y(i),vy(i),r(i),vr(i),th(i),
     &           vth(i))
c Create complex array variable:
                x1tmp = dble(r(i))
                x2tmp = dble(vr(i))
c first complex function: f_r = r+ivr:
                fkx(i) = cmplx(x1tmp,x2tmp)
                x1tmp = dble(th(i))
                x2tmp = dble(vth(i))
c Poincare's sympletic polar variables:
                psp1 =dsqrt(2.d0*dabs(x2tmp))
                psp2 =dcos(x1tmp)
                psp3 =dsin(x1tmp)
c second complex function
                fky(i) = cmplx(psp1*psp2,psp1*psp3)
c z- coordinate:
                x1tmp = dble(z(i))
                x2tmp = dble(vz(i))
                fkz(i) = cmplx(x1tmp,x2tmp)
         end do

  	 ti1 = getcpu()
c-------- Main frequency analysis routine: --------------------------------c
         write(8,*) 'orbit # ', partno(n)
         write(9,*) 'orbit # ', partno(n)

         call fundafreq(1,ntstep,nui,aai)
         write(41,103) partno(n), (nui(j), j=1,3), 
     &            (cdabs(aai(j)),j=1,3), etot(n)

 103     FORMAT(i5,1x,     7(e16.8, 1x))
         ti2 = getcpu()
         if(mod(n,100).eq.0) write(*,*) 'particle ', n,' T=',ti2-ti1
         ti1 = getcpu() 
        istat = 1
c End of loop over N (Particle number
      end do
      write(41,*) '                '
      timen = getcpu()-time0
c
c      close(31)
c      close(8)
c      close(9)
      close(41)
c------------------------------END OF MAIN -------------------------------c
c
      stop
      end

c Subroutine to convert Cartesian to Cylindrical polar coordinates
      subroutine cart2pol(x ,vx ,y ,vy ,r ,vr ,theta ,vtheta )
      implicit none
      real*4 x ,vx ,y ,vy ,r ,vr ,theta ,vtheta
c Following Papaphillipou & Laskar:         
c  r= sqrt(x**2+y**2), theta=atan2(y,x) 
c  vR= (x*Vx + y*Vy)/r , vTheta= x*Vy - yVx  
      r = sqrt(x**2+ y**2)
      theta=atan2(y,x)
      vR= (x*Vx + y*Vy)/r 
      vTheta= x*Vy - y*Vx  
      return
      end
