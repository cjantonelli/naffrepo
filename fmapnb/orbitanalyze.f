c Does analysis of orbits for bars and halos
Cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      Program orbitanalyze
c reads double precision arrays for x,y,z,vx,vy,vz: output is single precision
c Computes: nper, rapo, rperi, RCapo, RCperi, <x> xmax, <y>, ymax, <z>, zmax, 3 types of Jx, Jz, etot
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit none 
      integer i,j,k,m,n, npartmax, nstepmax,nnt,it
      integer ntstep,npart,ntactual,istart,nvar,nflag
      parameter (npartmax=50000, nstepmax=2000000, nvar=6)
      REAL*4 otime(nstepmax)
      real*4 X(nstepmax), Y(nstepmax), Z(nstepmax), vX(nstepmax),
     &       vY(nstepmax), VZ(nstepmax)
      REAL*4 etot
      integer partno(npartmax),nactual
      real*4 nper, rapo, rperi,RCap,RCper
      real*4 xmax, xmean, ymax, ymean,zmean,zmax,jz,jx, jy,
     &  jxmx, jzmx, Jxnt, Jznt,etotal(npartmax)
      common /steptim/ nactual, otime
      common /coordinates/etot, x,y,z,vx,vy,vz
      character*100 fileref
      CHARACTER*50 LINESTRING1
      character*50 linestring2, linestring3
      character*7 outfile
      CHARACTER*50 NAME
      character*50 linestring4, FileNames
      external analyze, ReadFile1
c-------------------------------------------------------------------------c
      write(*,*) 'filename for output data (max a7)'
      read(*,'(a)') outfile
      write(*,*) 'Number of particles?'
      read(*,*) npart
      write(*,*) 'give file that has names of all orbit files'
      read(*,'(a)') FileNames

c-------------------------------------------------------------------------c
      open(21, file =FileNames,status='old')

      open(9, file = outfile//'_AO.dat', status='unknown')
c                  0         1         2         3         4         5
c                  01234567890123456789012345678901234567890123456789
c      LINESTRING1='   N       n_per     r_apo      r_per    RC_apo   '   
c      Linestring2='   RC_per   <X>      Xmax       <Y>       Ymax    ' 
c      Linestring3='   <Z>     Zmax     <Jx>       <Jz>       Jxmx    '
c      Linestring4='   Jzmx    Jxnt      Jznt                         '
      write(9,102)'  N  ','n_per','r_apo','r_per','RC_ap','RC_pr',
     &    ' <X> ',' Xmax',' <Y> ',' Ymax', ' <Z> ',' Zmax',' <Jx>',
     &    ' <Jz>',' Jxmx',  ' Jzmx'   ,' Jxnt',' Jznt',' Etot'
c     write(9,*) linestring1, linestring2, linestring3, linestring4
c       write(*,*) 'give orbit number to start at? (1 for new file)'
c       read (*,*) istart
       istart=1
c-------------------------------------------------------------------------c

c Loop over particles:
      do N = istart, NPART
         read(21,'(a)') fileref
         if(n.eq.1. or. mod(n,100).eq.0)
     &           write(*,*) n, 'reading from ',fileref 
c READ input orbit:
         call ReadFile1(fileref)
         etotal(n) = etot
         partno(n) = n
         call analyze(nper,rapo,rperi,RCap,RCper,xmean,xmax,
     &       ymean,ymax,zmean,zmax,jx,jz,jxmx,jzmx,jxnt,jznt)
         write(9,103) n,nper,rapo, rperi, RCap,RCper,xmean,xmax,ymean,
     &    ymax,zmean,zmax, jx,jz,jxmx,jzmx,jxnt,jznt, Etotal(n)
 102      format(a6, 1x, 18(4x,a5,4x))

 103      format(i6, 1x, 18(e12.4,1x))
      end do
c
c End of loop over N (Particle number)
      close(21)
      close(9)
      stop
      end
c
c------------------------------END OF MAIN -------------------------------c
c Subroutine Analyze:
c this routine uses each entire orbit to do various things related to each orbit that are used in orbit classification:
c * find number of x-crossings, y-crossings, zcrossing and computes
c    nper = 0.5*min(xcross,ycross,zcross) ~ number of orbital periods 
c * find spherical r_peri, r_apo
c * find cylindrical Rc_peri, Rc_apo
c * find <x> and abs(xmax), <y> and abs(ymax), <z> and abs(zmax) 
c * find average angular momentum over all timesteps about x,z axes
c * find max angular momentum about x, z axes
c * find summed sign of angular momentum about x, z axes
c **********
      subroutine analyze(nper,rapo,rperi,RCap,RCper,xmean,xmax
     &  ,ymean,ymax,zmean,zmax,jxav,jzav,jxmx,jzmx,jxnt,jznt)
      implicit none
      integer nstep, n, i, j,k, nstepmax, signx,signz
      parameter (nstepmax=2000000)
      real*4 nper,rperi,rapo, RCap, RCper,
     & zmean,zmax,xmean,xmax, ymean, ymax
      real*4  Rci,ri, zi, xi, yi, sign,jxmx, jxav
      real*4 xcross, ycross, zcross, jx(nstepmax), jz(nstepmax)
      real*4 x(nstepmax), y(nstepmax), z(nstepmax)
      real*4 vx(nstepmax), vy(nstepmax), vz(nstepmax)
      real*4 jzmx, jzav,jxnt,jznt, t(nstepmax),etot
      external sign
      common /steptim/ nstep, t
      common /coordinates/etot, x,y,z,vx,vy,vz

      rperi = 1.e+12
      rapo = 0.
      RCper = 1.e+12
      RCap = 0.
      xmean=0.
      xmax=1.e-9
      ymean=0.
      ymax=1.e-9
      zmean=0.
      zmax=1.e-9
      xcross = 0.
      ycross = 0.
      zcross = 0.
      nper = 0.

      xi = 0.
      yi = 0.
      zi = 0.
      jxav=0.
      jzav=0.
      jxmx=0.
      jzmx=0.
      jxnt=0.
      jznt=0.

c begin loop over timesteps:
      do n = 1,nstep-1
c number of xcrossings, ycrossings, zcrossings:
         if(sngl(x(n)*x(n+1)).lt.0.d0) xcross = xcross + 1.
         if(sngl(y(n)*y(n+1)).lt.0.d0) ycross = ycross + 1.
         if(sngl(z(n)*z(n+1)).lt.0.d0) zcross = zcross + 1.
c Cylindrical rapo and rperi:
         Rci = sqrt(sngl(x(n)**2 + y(n)**2))
         ri = sqrt(Rci**2 + sngl(z(n)**2))
         xi = xi + abs(sngl(x(n)))
         yi = yi + abs(sngl(y(n)))
         zi = zi + abs(sngl(z(n)))
         rperi = min(rperi, ri)
         rapo = max(rapo, ri)
         RCper = min(RCper, Rci)
         RCap = max(RCap, Rci)

c Angular momenta:
         jx(n) = sngl(y(n)*vz(n) - z(n)*vy(n))
         jz(n) = sngl(x(n)*vy(n) - y(n)*vx(n))
         Jxav = jxav+jx(n)
         Jzav = jzav+jz(n)

         signz = sign(jz(n))
         jznt=jznt+signz
         if(abs(jz(n)).gt.abs(jzmx)) then
            jzmx = jz(n)
         end if
c         
         signx = sign(jx(n))
         jxnt=jxnt+signx
         if(abs(jx(n)).gt.abs(jxmx)) then
            jxmx = jx(n)
         end if

c Coordinate max:
         xmax = max(xmax,abs(sngl(x(n))))
         ymax = max(ymax,abs(sngl(y(n))))
         zmax = max(zmax,abs(sngl(z(n))))
c end loop over timesteps:
      end do

c Coordinate mean:
      xmean =  xi/float(nstep-1)
      ymean =  yi/float(nstep-1)
      zmean =  zi/float(nstep-1)

c average of angular momentum over the entire orbit:
      jxav = jxav/float(nstep-1)
      jzav = jzav/float(nstep-1)
c Net Jx,Jz sign:
      jxnt = jxnt/float(nstep-1)
      jznt = jznt/float(nstep-1)

c      write(*,*) 'xcross, ycr, zcr', xcross, ycross, zcross
      nper = min(xcross, ycross)
      nper = min(nper, zcross)
      nper = 0.5*nper
c
      return
      end
c---------------------------------------------------------
      real*4 function sign(x)
      real*4 x
      
      sign = 1.
      if(x.le.0) sign= -1.
      
      return
      end
c---------------------------------------------------------

