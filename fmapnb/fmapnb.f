Cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      Program FMAP
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c version 4.1: version that allows each orbit to be in a separate file
c              and holds only one orbit in memory at a time. Also does
c              only one phase at at time. (Sept 2009)
c version 4.0a: version that allows each phase to have slightly different
c               number of time steps - to accomodate the 43M steps in PlA3
c Version 4.0: analyze a  single orbit from an N-body simulation (sept 2008)
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c OUTLINE for Version 4 (pared down)
c ----------
C 1)  read each orbit from datafile and create complex arrays
c 2)  For two successive time intervals call FUNDAFREQ:
c     a} Find frequencies and amplitudes of each orbit,(FRECODER)
c     b} Identify Fundamental frequencies (INTEGFREQ)
c     c} Calculate integers that correspond to each frequency (INTEGFREQ)
c 5)  OUTPUT FILES:
c     filename.out -- all inout data, freq, complex amplitudes for each comp
c     filename.frq -- frequencies and amplitudes
c     filename.flg -- flag file to terminate prog if necessary 
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c Variable declaration:
      implicit none 
      include '../naff_npg/fmarray.f'
      integer i,j,m,n,np,istat,tsl,irs,nflg, nfiletyp
      integer nvar, splot,npl, itmp, npart, ntstep, istart
      real*8 ti1,ti2
      real*8 nui(3), getcpu,time0,timen     
      real*8 x1tmp, x2tmp
      complex*16 aai(3)
c arrays for storing fundamental frequencies:
      integer partno(npartmax), nactualsteps
      parameter (nvar=6)    
      real x(nbmax), y(nbmax), z(nbmax)
      real vx(nbmax), vy(nbmax), vz(nbmax)
      real etot(npartmax),ttmp(nbmax), etotal
      character*100 fileref
      CHARACTER*50 LINESTRING1, linestring2
      character*50 FileNames
      common /state/ istat
      common /timslice/ tsl
      common /steptim/ ntstep, ttmp
      common /coordinates/ etotal,x,y,z,vx,vy,vz
      external fundafreq, getcpu, ReadFile1

c--------------------------------------------------------------------------c
      write(*,*) 'File name for frequency output (a7 e.g. run1001)'
      read(*,'(a)') freqfile

      write(*,*) 'Number of test-particles for which you have orbits?'
      read(*,*) npart
      IF(NPART.GT.NPARTMAX) STOP 
     &   'Too big! Increase npartmax in /naffrepo/naff_npg/fmarray.f'

      istat = 0
      time0 = getcpu()
c-------------------------------------------------------------------------c
      write(*,*) 'give file with names of orbit files (with full path)'
      write(*,*) 'e.g. run1001.filenames'
      read(*,'(a)') FileNames
      write(*,*) 'reading orbit file names from ', Filenames

      open(21, file =FileNames,status='old')
      open(41, file=trim(freqfile)//'_ffrq.dat',status='unknown')
      write(41,102)'  N ', ' w_x',' w_y',' w_z',' A_x',' A_y',
     &            ' A_z','Etot'
c File with nvec frequencies for each component  and corresponding integers:
       open(8,file=trim(freqfile)//'.int',status='unknown')
       open(9,file=trim(freqfile)//'.out' ,status='unknown')
       write(*,*) 'give orbit number to start at? (1 for new file)'
       read (*,*) istart

c Skip over particles if istart.ne.1:
       if(istart.ne.1) then
          do n = 1,istart-1
                read(21,'(a)') fileref
                write(*,*) 'skipping file ', n, fileref
         end do
      end if

c Loop over particles:
      do N = istart, NPART
         read(21,'(a)') fileref
         if(n.eq.1. or. mod(n,5).eq.0) write(*,*) 'n ', n, ' ',fileref
         open(2101, file=freqfile//'.flg',status='unknown')
         write(2101,*) n, 'reading from ',fileref 
c READ input orbit:
         call ReadFile1(fileref)
         etot(n) = etotal
         partno(n) = n
	 if(ntstep.gt.nbmax) stop 'increase nbmax'
c         write(*,*) 'timesteps per orbit', ntstep
	 do i = 1,ntstep
                tout(i) = dble(ttmp(i))
c Create complex array variable
                x1tmp = dble(x(i))
                x2tmp = dble(vx(i))
                fkx(i) = cmplx(x1tmp,x2tmp)

                x1tmp = dble(y(i))
                x2tmp = dble(vy(i))
                fky(i) = cmplx(x1tmp,x2tmp)

                x1tmp = dble(z(i))
                x2tmp = dble(vz(i))
                fkz(i) = cmplx(x1tmp,x2tmp)
         end do

  	 ti1 = getcpu()
c-------- Main frequency analysis routine: --------------------------------c
         write(8,*) 'orbit # ', partno(n)
         write(9,*) 'orbit # ', partno(n)
         call fundafreq(1,ntstep,nui,aai)

         write(41,103) partno(n), (nui(j), j=1,3), 
     &            (cdabs(aai(j)),j=1,3), etot(n)
 102     Format(1x, a4,1x, 7(6x, a4, 7x))
 103     FORMAT(i5,1x,     7(e16.8, 1x))
         ti2 = getcpu()
         if(mod(n,100).eq.0) write(*,*) 'particle ', n,' T=',ti2-ti1
         ti1 = getcpu() 
        istat = 1
c End of loop over N (Particle number
      end do
      write(41,*) '                '
      timen = getcpu()-time0
c
c      close(31)
      close(8)
      close(9)
      close(41)
c------------------------------END OF MAIN -------------------------------c
c
      stop
      end
