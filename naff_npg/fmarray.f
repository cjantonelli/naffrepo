c----------------------------------------------------------------------
c fmarray.f 
c common arrays for Frequency Analysis Routines in naff_npg
c----------------------------------------------------------------------
c Parameters to be set at input:
c     nbmax: maximum number of steps for which orbit is integrated
c     nvec: number of frequency components in each dimension (X,y,z)
      integer nbmax, nvec, npartmax
c YOU CAN CHANGE THESE
      parameter (nbmax=2000000, nvec=10, npartmax=10000)    
c*********************************************************************\\
c Nothing to be changed below
      real*8 pi
      real*8 tz(nbmax),chi(nbmax),tg,konst
      real*8 tmax, tout(nbmax)
      parameter (pi = 3.141592653589793238462643d0)
      complex*16 fkx(nbmax), fky(nbmax), fkz(nbmax)
      complex*16 ecap(nvec,nbmax)
      logical stat
      character*7  freqfile
      common /init/ tmax
      common /arrays/ tz,chi,ecap
      common /params/ tg,konst
      common /status/ stat
      common /ffunc/ fkx,fky,fkz,tout
      common /files/ freqfile
c*********************************************************************\\
