.PHONY: progs clean syslib syslib-clean

progs:
	cd syslib; make install
	cd naff_npg; make
	cd fmapnb; make
	cd plotNclassify; make
	@echo
	@echo "Add the following two lines to your .bashrc and logout and login to enable:"
	@echo "export LD_LIBRARY_PATH=`pwd`/local/lib:\$$LD_LIBRARY_PATH"
	@echo "export PGPLOT_DIR=`pwd`/local/pgplot"

test:
	cp -r Examples/test001 .
	cd test001; bash runfreqall.s; bash plotNclass.s
	@echo
	@echo Results are in directory test001

progs-clean:
	cd syslib; make clean
	cd naff_npg; make clean
	cd fmapnb; make clean
	cd plotNclassify; make clean
