FC=gfortran

MODULES = glpk_types.mod glpk_constants.mod glpk.mod

all: $(MODULES)

glpk_types.mod: glpk_types.f90
	${FC} ${FFLAGS} -c $<
glpk_constants.mod: glpk_constants.f90
	${FC} ${FFLAGS} -c $<
glpk.mod: glpk.f90 glpk_types.mod glpk_constants.mod
	${FC} ${FFLAGS} -c $<

install:
	cp -p $(MODULES) ../../../local/include

clean:
	/bin/rm -f $(MODULES) *.o
