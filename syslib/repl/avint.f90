! https://people.sc.fsu.edu/~jburkardt/f_src/intlib/intlib.html

subroutine avint ( ntab, xtab, ytab, a, b, result )

!*****************************************************************************80
!
!! AVINT estimates the integral of unevenly spaced data.
!
!  Discussion:
!
!    The data is given as NTAB pairs of values 
!    ( XTAB(1:NTAB), YTAB(1:NTAB) ).
!
!    The quadrature method uses overlapping parabolas and smoothing.
!
!  Modified:
!
!    10 February 2006
!
!  Reference:
!
!    Philip Davis, Philip Rabinowitz,
!    Methods of Numerical Integration,
!    Second Edition,
!    Dover, 2007,
!    ISBN: 0486453391,
!    LC: QA299.3.D28.
!
!    Paul Hennion,
!    Algorithm 77:
!    Interpolation, Differentiation and Integration,
!    Communications of the ACM,
!    Volume 5, page 96, 1962.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NTAB, the number of entries in XTAB and
!    YTAB.  NTAB must be at least 2.
!
!    Input, real ( kind = 8 ) XTAB(NTAB), the abscissas at which the
!    function values are given.  The XTAB's must be distinct
!    and in ascending order.
!
!    Input, real ( kind = 8 ) YTAB(NTAB), the function values,
!    YTAB(I) = F(XTAB(I)).
!
!    Input, real ( kind = 8 ) A, the lower limit of integration.  A
!    should
!    be, but need not be, near one endpoint of the interval
!    (X(1), X(NTAB)).
!
!    Input, real ( kind = 8 ) B, the upper limit of integration.  B
!    should
!    be, but need not be, near one endpoint of the interval
!    (X(1), X(NTAB)).
!
!    Output, real ( kind = 8 ) RESULT, the approximate value of the
!    integral.
!
  implicit none

  integer ( kind = 4 ) ntab

  real ( kind = 8 ) a
  real ( kind = 8 ) b
  real ( kind = 8 ) ba
  real ( kind = 8 ) bb
  real ( kind = 8 ) bc
  real ( kind = 8 ) ca
  real ( kind = 8 ) cb
  real ( kind = 8 ) cc
  real ( kind = 8 ) fa
  real ( kind = 8 ) fb
  integer ( kind = 4 ) i
  integer ( kind = 4 ) inlft
  integer ( kind = 4 ) inrt
  integer ( kind = 4 ) istart
  integer ( kind = 4 ) istop
  real ( kind = 8 ) result
  real ( kind = 8 ) slope
  real ( kind = 8 ) syl
  real ( kind = 8 ) syl2
  real ( kind = 8 ) syl3
  real ( kind = 8 ) syu
  real ( kind = 8 ) syu2
  real ( kind = 8 ) syu3
  real ( kind = 8 ) term1
  real ( kind = 8 ) term2
  real ( kind = 8 ) term3
  real ( kind = 8 ) total
  real ( kind = 8 ) x1
  real ( kind = 8 ) x12
  real ( kind = 8 ) x13
  real ( kind = 8 ) x2
  real ( kind = 8 ) x23
  real ( kind = 8 ) x3
  real ( kind = 8 ) xtab(ntab)
  real ( kind = 8 ) ytab(ntab)

  result = 0.0D+00

  if ( a == b ) then
    return
  end if

  if ( b < a ) then
  end if

  if ( ntab < 2 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'AVINT - Fatal error!'
    write ( *, '(a,i8)' ) '  NTAB is less than 3.  NTAB = ', ntab
    stop 1
  end if

  do i = 2, ntab
 
    if ( xtab(i) <= xtab(i-1) ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'AVINT - Fatal error!'
      write ( *, '(a)' ) '  XTAB(I) is not greater than XTAB(I-1).'
      write ( *, '(a,i8)' ) '  Here, I = ', I
      write ( *, '(a,g14.6)' ) '  XTAB(I-1) = ', xtab(i-1)
      write ( *, '(a,g14.6)' ) '  XTAB(I) =   ', xtab(i)
      stop 1
    end if
 
  end do
!
!  Special case for NTAB = 2.
!
  if ( ntab == 2 ) then
    slope = ( ytab(2) - ytab(1) ) / ( xtab(2) - xtab(1) )
    fa = ytab(1) + slope * ( a - xtab(1) )
    fb = ytab(2) + slope * ( b - xtab(2) )
    result = 0.5D+00 * ( fa + fb ) * ( b - a )
    return
  end if

  if ( xtab(ntab-2) < a .or. b < xtab(3) ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'AVINT - Fatal error!'
    write ( *, '(a)' ) '  There were less than 3 function values'
    write ( *, '(a)' ) '  between the limits of integration.'
    stop 1
  end if

  i = 1
  do

    if ( a <= xtab(i) ) then
      exit
    end if

    i = i + 1

  end do

  inlft = i

  i = ntab

  do

    if ( xtab(i) <= b ) then
      exit
    end if

    i = i - 1

  end do

  inrt = i

  if ( inrt - inlft < 2 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'AVINT - Fatal error!'
    write ( *, '(a)' ) '  There were less than 3 function values'
    write ( *, '(a)' ) '  between the limits of integration.'
    stop 1
  end if

  if ( inlft == 1 ) then
    istart = 2
  else
    istart = inlft
  end if

  if ( inrt == ntab ) then
    istop = ntab - 1
  else
    istop = inrt
  end if

  total = 0.0D+00

  syl = a
  syl2 = syl * syl
  syl3 = syl2 * syl

  do i = istart, istop

    x1 = xtab(i-1)
    x2 = xtab(i)
    x3 = xtab(i+1)

    x12 = x1 - x2
    x13 = x1 - x3
    x23 = x2 - x3

    term1 =   ( ytab(i-1) ) / ( x12 * x13 )
    term2 = - ( ytab(i)   ) / ( x12 * x23 )
    term3 =   ( ytab(i+1) ) / ( x13 * x23 )

    ba = term1 + term2 + term3
    bb = - ( x2 + x3 ) * term1 - ( x1 + x3 ) * term2 - ( x1 + x2 ) * term3
    bc = x2 * x3 * term1 + x1 * x3 * term2 + x1 * x2 * term3

    if ( i == istart ) then
      ca = ba
      cb = bb
      cc = bc
    else
      ca = 0.5D+00 * ( ba + ca )
      cb = 0.5D+00 * ( bb + cb )
      cc = 0.5D+00 * ( bc + cc )
    end if

    syu = x2
    syu2 = syu * syu
    syu3 = syu2 * syu

    total = total + ca * ( syu3 - syl3 ) / 3.0D+00 &
                  + cb * ( syu2 - syl2 ) / 2.0D+00 &
                  + cc * ( syu  - syl )
    ca = ba
    cb = bb
    cc = bc

    syl  = syu
    syl2 = syu2
    syl3 = syu3

  end do

  syu = b
  syu2 = syu * syu
  syu3 = syu2 * syu

  result = total + ca * ( syu3 - syl3 ) / 3.0D+00 &
                 + cb * ( syu2 - syl2 ) / 2.0D+00 &
                 + cc * ( syu  - syl  )
  
  return
end
