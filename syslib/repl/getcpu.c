// return consumed cpu time, with nanosecond granularity

#include <stdio.h>
#include <time.h>

double getcpu_()
{
	struct timespec ts;

	if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts) < 0)
		return 0;

	return ts.tv_sec + ts.tv_nsec*1e-09;
}
